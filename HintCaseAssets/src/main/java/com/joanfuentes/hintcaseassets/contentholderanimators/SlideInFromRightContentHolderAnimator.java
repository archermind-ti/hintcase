package com.joanfuentes.hintcaseassets.contentholderanimators;

import com.joanfuentes.hintcase.ContentHolderAnimator;
import com.joanfuentes.hintcase.utils.ComponentUtils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

public class SlideInFromRightContentHolderAnimator extends ContentHolderAnimator {

    AnimatorValue animator;

    public SlideInFromRightContentHolderAnimator() {
        super();
        animator = new AnimatorValue();
        animator.setDuration(durationInMilliseconds);
        animator.setDelay(startDelayInMilliseconds);
        animator.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
    }

    public SlideInFromRightContentHolderAnimator(int durationInMilliseconds) {
        super(durationInMilliseconds);
    }

    @Override
    public Animator getAnimator(final Component component, final OnFinishListener onFinishListener) {
        final int distance = ComponentUtils.getRootComponent(component).getRight() - component.getLeft();
        animator.setValueUpdateListener((animatorValue, value) -> {
            component.setTranslationX(distance * (1 - value));
            if (component.getVisibility() != Component.VISIBLE) {
                component.setVisibility(Component.VISIBLE);
            }
            if (value == 1 && onFinishListener != NO_CALLBACK) {
                onFinishListener.onFinish();
            }
        });
        return animator;
    }
}
