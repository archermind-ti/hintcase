package com.joanfuentes.hintcaseassets.contentholderanimators;


import com.joanfuentes.hintcase.ContentHolderAnimator;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

public class FadeOutContentHolderAnimator extends ContentHolderAnimator {

    public FadeOutContentHolderAnimator() {
        super();
    }

    public FadeOutContentHolderAnimator(int durationInMilliseconds) {
        super(durationInMilliseconds);
    }

    @Override
    public Animator getAnimator(Component component, final OnFinishListener onFinishListener) {
        AnimatorProperty animator = new AnimatorProperty(component).alphaFrom(1).alpha(0);
        animator.setDuration(durationInMilliseconds);
        animator.setDelay(startDelayInMilliseconds);
        animator.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        animator.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                component.setVisibility(Component.INVISIBLE);
                if (onFinishListener != NO_CALLBACK) {
                    onFinishListener.onFinish();
                }
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        return animator;
    }
}
