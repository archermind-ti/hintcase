package com.joanfuentes.hintcaseassets.contentholderanimators;

import com.joanfuentes.hintcase.ContentHolderAnimator;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

public class FadeInContentHolderAnimator extends ContentHolderAnimator {

    public FadeInContentHolderAnimator() {
        super();
    }

    public FadeInContentHolderAnimator(int durationInMilliseconds) {
        super(durationInMilliseconds);
    }

    @Override
    public Animator getAnimator(Component component, final OnFinishListener onFinishListener) {
        component.setVisibility(Component.VISIBLE);
        AnimatorProperty animator = new AnimatorProperty(component).alphaFrom(0).alpha(1);
        animator.setDuration(durationInMilliseconds);
        animator.setDelay(startDelayInMilliseconds);
        animator.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        if (onFinishListener != NO_CALLBACK) {
            animator.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {
                }

                @Override
                public void onStop(Animator animator) {
                }

                @Override
                public void onCancel(Animator animator) {
                }

                @Override
                public void onEnd(Animator animator) {
                    onFinishListener.onFinish();
                }

                @Override
                public void onPause(Animator animator) {
                }

                @Override
                public void onResume(Animator animator) {
                }
            });
        }
        return animator;
    }
}
