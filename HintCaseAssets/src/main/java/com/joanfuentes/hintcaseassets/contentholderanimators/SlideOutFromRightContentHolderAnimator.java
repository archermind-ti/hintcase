package com.joanfuentes.hintcaseassets.contentholderanimators;

import com.joanfuentes.hintcase.ContentHolderAnimator;
import com.joanfuentes.hintcase.utils.ComponentUtils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

public class SlideOutFromRightContentHolderAnimator extends ContentHolderAnimator {

    public SlideOutFromRightContentHolderAnimator() {
        super();
    }

    public SlideOutFromRightContentHolderAnimator(int durationInMilliseconds) {
        super(durationInMilliseconds);
    }

    @Override
    public Animator getAnimator(final Component component, final OnFinishListener onFinishListener) {
        AnimatorProperty animator = new AnimatorProperty(component).moveFromX(component.getLeft()).moveToX(ComponentUtils.getRootComponent(component).getRight());
        animator.setDuration(durationInMilliseconds);
        animator.setDelay(startDelayInMilliseconds);
        animator.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        animator.setStateChangedListener(new Animator.StateChangedListener() {
            @Override
            public void onStart(Animator animator) {
                component.setVisibility(Component.VISIBLE);
            }

            @Override
            public void onStop(Animator animator) {
            }

            @Override
            public void onCancel(Animator animator) {
            }

            @Override
            public void onEnd(Animator animator) {
                if (onFinishListener != NO_CALLBACK) {
                    onFinishListener.onFinish();
                }
            }

            @Override
            public void onPause(Animator animator) {
            }

            @Override
            public void onResume(Animator animator) {
            }
        });
        return animator;
    }
}
