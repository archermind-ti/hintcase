package com.joanfuentes.hintcaseassets.shapes;


import com.joanfuentes.hintcase.Shape;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.Path;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

public class CircularShape extends Shape {
    private static final float DEFAULT_MIN_RADIUS = 50;
    private static final float DEFAULT_MAX_RADIUS = 10000;

    private float minRadius = DEFAULT_MIN_RADIUS;
    private float maxRadius = DEFAULT_MAX_RADIUS;
    private float currentRadius = DEFAULT_MAX_RADIUS;

    public float getMinRadius() {
        return minRadius;
    }

    public float getMaxRadius() {
        return maxRadius;
    }

    public float getCurrentRadius() {
        return currentRadius;
    }

    public void setCurrentRadius(float currentRadius) {
        this.currentRadius = currentRadius;
    }

    @Override
    public void setMinimumValue() {
        currentRadius = minRadius;
    }

    @Override
    public void setShapeInfo(Component targetComponent, ComponentContainer parent, int offset, Context context) {
        if (targetComponent != null && parent != null) {
            minRadius = (Math.max(targetComponent.getEstimatedWidth(), targetComponent.getEstimatedHeight()) / 2f) + offset;
            maxRadius = Math.max(parent.getHeight(), parent.getWidth());
            int[] targetComponentLocationInWindow = targetComponent.getLocationOnScreen();
            int[] parentLocationInWindow = parent.getLocationOnScreen();
            targetComponentLocationInWindow[0] = targetComponentLocationInWindow[0] - parentLocationInWindow[0];
            targetComponentLocationInWindow[1] = targetComponentLocationInWindow[1] - parentLocationInWindow[1];
            setCenterX(targetComponentLocationInWindow[0] + targetComponent.getWidth() / 2f);
            setCenterY(targetComponentLocationInWindow[1] + targetComponent.getHeight() / 2f);
            setLeft((int) (getCenterX() - minRadius));
            setRight((int) (getCenterX() + minRadius));
            setTop((int) (getCenterY() - minRadius));
            setBottom((int) (getCenterY() + minRadius));
            setWidth(minRadius * 2);
            setHeight(minRadius * 2);
        } else if (parent != null) {
            minRadius = 0;
            maxRadius = parent.getHeight();
            setCenterX(parent.getEstimatedWidth() / 2f);
            setCenterY(parent.getEstimatedHeight() / 2f);
            setLeft(0);
            setTop(0);
            setRight(0);
            setBottom(0);
        }
        currentRadius = maxRadius;
    }

    @Override
    public boolean isTouchEventInsideTheHint(TouchEvent event) {
        MmiPoint point = event.getPointerPosition(event.getIndex());
        float xDelta = Math.abs(point.getX() - getCenterX());
        float yDelta = Math.abs(point.getY() - getCenterY());
        double distanceFromFocus = Math.sqrt(Math.pow(xDelta, 2) + Math.pow(yDelta, 2));
        return distanceFromFocus <= minRadius;
    }

    @Override
    public void draw(Canvas canvas) {
        Path p = getShapePath();
        p.reset();
        p.addCircle(getCenterX(), getCenterY(), currentRadius, Path.Direction.CLOCK_WISE);
        canvas.clipPath(p, Canvas.ClipOp.DIFFERENCE);
    }
}
