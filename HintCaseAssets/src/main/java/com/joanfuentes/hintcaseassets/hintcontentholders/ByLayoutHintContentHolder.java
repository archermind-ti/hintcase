package com.joanfuentes.hintcaseassets.hintcontentholders;

import com.joanfuentes.hintcase.HintCase;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;
import ohos.agp.components.LayoutScatter;

public class ByLayoutHintContentHolder extends HintContentHolder {
    final int resLayoutId;

    public ByLayoutHintContentHolder(int resLayoutId) {
        super();
        this.resLayoutId = resLayoutId;
    }

    @Override
    public Component getComponent(Context context, HintCase hintCase, ComponentContainer parent) {
        LayoutScatter layoutScatter = LayoutScatter.getInstance(context);
        return layoutScatter.parse(resLayoutId, parent, false);
    }
}
