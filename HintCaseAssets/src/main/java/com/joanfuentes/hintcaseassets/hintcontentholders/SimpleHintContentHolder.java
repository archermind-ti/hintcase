package com.joanfuentes.hintcaseassets.hintcontentholders;

import com.joanfuentes.hintcase.HintCase;
import com.joanfuentes.hintcase.utils.ComponentUtils;
import ohos.agp.components.*;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.media.image.common.Size;

import java.io.IOException;

public class SimpleHintContentHolder extends HintContentHolder {
    public static final int NO_IMAGE = -1;

    private Image image;
    private int imageResourceId;
    private String contentTitle;
    private String contentText;
    private int titleStyleId;
    private int textStyleId;
    private int marginLeft;
    private int marginTop;
    private int marginRight;
    private int marginBottom;
    private int alignment;

    @Override
    public Component getComponent(Context context, final HintCase hintCase, ComponentContainer parent) {
        StackLayout.LayoutConfig stackLayoutConfigBlock = getParentLayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT,
                alignment, marginLeft, marginTop, marginRight, marginBottom);
        DirectionalLayout directionalLayout = new DirectionalLayout(context);
        directionalLayout.setLayoutConfig(stackLayoutConfigBlock);
        directionalLayout.setAlignment(LayoutAlignment.CENTER);
        directionalLayout.setOrientation(DirectionalLayout.VERTICAL);
        if (contentTitle != null) {
            directionalLayout.addComponent(getTextTitle(context));
        }
        if (existImage()) {
            directionalLayout.addComponent(getImage(context, parent, image, imageResourceId));
        }
        if (contentText != null) {
            directionalLayout.addComponent(getTextDescription(context));
        }
        return directionalLayout;
    }

    private Component getImage(Context context, ComponentContainer parent, Image image, int imageResourceId) {
        Size size = new Size(1, 1);
        int width = parent.getWidth() - marginLeft - marginRight;
        if (image == null) {
            image = new Image(context);
        }
        if (imageResourceId != SimpleHintContentHolder.NO_IMAGE) {
            image.setPixelMap(imageResourceId);
        }
        if (image.getPixelMap() != null) {
            size = image.getPixelMap().getImageInfo().size;
        }
        DirectionalLayout.LayoutConfig directionalLayoutConfigImage = new DirectionalLayout.LayoutConfig(width, width * size.height / size.width);
        directionalLayoutConfigImage.setMargins(0, 20, 0, 50);
        image.setScaleMode(Image.ScaleMode.STRETCH);
        image.setLayoutConfig(directionalLayoutConfigImage);
        return image;
    }

    private Component getTextTitle(Context context) {
        DirectionalLayout.LayoutConfig directionalLayoutConfigTitle =
                new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        directionalLayoutConfigTitle.setMargins(0, 20, 0, 20);
        Text textTitle = new Text(context);
        textTitle.setLayoutConfig(directionalLayoutConfigTitle);
        textTitle.setTextAlignment(TextAlignment.HORIZONTAL_CENTER);
        textTitle.setText(contentTitle);
        ComponentUtils.applyStyle(context, textTitle, titleStyleId);
        return textTitle;
    }

    private Component getTextDescription(Context context) {
        DirectionalLayout.LayoutConfig directionalLayoutConfigText =
                new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                        ComponentContainer.LayoutConfig.MATCH_CONTENT);
        Text textDescription = new Text(context);
        textDescription.setLayoutConfig(directionalLayoutConfigText);
        textDescription.setTextAlignment(TextAlignment.HORIZONTAL_CENTER);
        textDescription.setText(contentText);
        textDescription.setMultipleLine(true);
        ComponentUtils.applyStyle(context, textDescription, textStyleId);
        return textDescription;
    }

    private boolean existImage() {
        return image != null ||
                imageResourceId != SimpleHintContentHolder.NO_IMAGE;
    }

    public static class Builder {
        private final SimpleHintContentHolder blockInfo;
        private final Context context;

        public Builder(Context context) {
            this.context = context;
            this.blockInfo = new SimpleHintContentHolder();
            this.blockInfo.imageResourceId = NO_IMAGE;
            this.blockInfo.alignment = LayoutAlignment.CENTER;
        }

        public Builder setImageResourceId(int resourceId) {
            blockInfo.imageResourceId = resourceId;
            return this;
        }

        public Builder setImage(Image image) {
            blockInfo.image = image;
            return this;
        }

        public Builder setContentTitle(String title) {
            blockInfo.contentTitle = title;
            return this;
        }

        public Builder setContentTitle(int resId) {
            blockInfo.contentTitle = context.getString(resId);
            return this;
        }

        public Builder setTitleStyle(int resId) {
            blockInfo.titleStyleId = resId;
            return this;
        }

        public Builder setContentText(String text) {
            blockInfo.contentText = text;
            return this;
        }

        public Builder setContentText(int resId) {
            blockInfo.contentText = context.getString(resId);
            return this;
        }

        public Builder setContentStyle(int resId) {
            blockInfo.textStyleId = resId;
            return this;
        }

        public Builder setAlignment(int alignment) {
            blockInfo.alignment = alignment;
            return this;
        }

        public Builder setMargin(int left, int top, int right, int bottom) {
            blockInfo.marginLeft = left;
            blockInfo.marginTop = top;
            blockInfo.marginRight = right;
            blockInfo.marginBottom = bottom;
            return this;
        }

        public Builder setMarginByResourcesId(int left, int top, int right, int bottom) {
            try {
                blockInfo.marginLeft = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(left).getInteger());
                blockInfo.marginTop = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(top).getInteger());
                blockInfo.marginRight = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(right).getInteger());
                blockInfo.marginBottom = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(bottom).getInteger());
            } catch (IOException | NotExistException | WrongTypeException e) {
                e.printStackTrace();
            }
            return this;
        }

        public SimpleHintContentHolder build() {
            return blockInfo;
        }
    }
}
