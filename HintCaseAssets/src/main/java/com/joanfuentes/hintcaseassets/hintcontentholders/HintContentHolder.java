package com.joanfuentes.hintcaseassets.hintcontentholders;

import com.joanfuentes.hintcase.ContentHolder;
import ohos.agp.components.StackLayout;

public abstract class HintContentHolder extends ContentHolder {
    public StackLayout.LayoutConfig getParentLayoutConfig(int width, int height, int alignment) {
        return new StackLayout.LayoutConfig(width, height, alignment);
    }

    public StackLayout.LayoutConfig getParentLayoutConfig(int width, int height, int alignment,
                                                          int marginLeft, int marginTop,
                                                          int marginRight, int marginBottom) {
        StackLayout.LayoutConfig layoutConfig = new StackLayout.LayoutConfig(width, height, alignment);
        layoutConfig.setMargins(marginLeft, marginTop, marginRight, marginBottom);
        return layoutConfig;
    }
}
