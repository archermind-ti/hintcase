package com.joanfuentes.hintcaseassets.extracontentholders;


import com.joanfuentes.hintcase.ContentHolder;
import ohos.agp.components.DependentLayout;

public abstract class ExtraContentHolder extends ContentHolder {
    public DependentLayout.LayoutConfig getParentLayoutConfig(int width, int height, int... rules) {
        DependentLayout.LayoutConfig layoutConfig =
                new DependentLayout.LayoutConfig(width, height);
        for (int rule : rules) {
            layoutConfig.addRule(rule);
        }
        return layoutConfig;
    }
}
