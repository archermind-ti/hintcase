package com.joanfuentes.hintcaseassets.extracontentholders;

import com.joanfuentes.hintcase.HintCase;
import com.joanfuentes.hintcase.utils.ComponentUtils;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.Arrays;

public class SimpleButtonContentHolder extends ExtraContentHolder {
    private int width;
    private int height;
    private int[] rules;
    private String text;
    private int buttonStyleId;
    private boolean closeHintOnClick;
    private OnClickButtonListener onClickButtonListener;

    SimpleButtonContentHolder() {
        this.closeHintOnClick = false;
    }

    @Override
    public Component getComponent(Context context, final HintCase hintCase, ComponentContainer parent) {
        Button button;
        button = new Button(context);
        if (buttonStyleId != 0) {
            ComponentUtils.applyStyle(context, button, buttonStyleId);
        }
        button.setText(text);
        button.setClickedListener(v -> {
            if (onClickButtonListener != null) {
                onClickButtonListener.onClick();
            }
            if (closeHintOnClick) {
                hintCase.hide();
            }
        });
        button.setLayoutConfig(getParentLayoutConfig(width, height, rules));
        return button;
    }

    public static class Builder {
        private final SimpleButtonContentHolder buttonBlock;
        private final Context context;

        public Builder(Context context) {
            this.context = context;
            buttonBlock = new SimpleButtonContentHolder();
            buttonBlock.width = ComponentContainer.LayoutConfig.MATCH_CONTENT;
            buttonBlock.height = ComponentContainer.LayoutConfig.MATCH_CONTENT;
            buttonBlock.rules = new int[0];
        }

        public Builder setWidth(int width) {
            buttonBlock.width = width;
            return this;
        }

        public Builder setHeight(int height) {
            buttonBlock.height = height;
            return this;
        }

        public Builder setRules(int... rules) {
            buttonBlock.rules = Arrays.copyOf(rules, rules.length);
            return this;
        }

        public Builder setButtonText(String text) {
            buttonBlock.text = text;
            return this;
        }

        public Builder setButtonText(int resId) {
            buttonBlock.text = context.getString(resId);
            return this;
        }

        public Builder setButtonStyle(int resId) {
            buttonBlock.buttonStyleId = resId;
            return this;
        }

        public Builder setOnClick(OnClickButtonListener listener) {
            buttonBlock.onClickButtonListener = listener;
            return this;
        }

        public Builder setCloseHintCaseOnClick(boolean closeHintOnClick) {
            buttonBlock.closeHintOnClick = closeHintOnClick;
            return this;
        }

        public SimpleButtonContentHolder build() {
            return buttonBlock;
        }
    }

    public interface OnClickButtonListener {
        void onClick();
    }
}
