package com.joanfuentes.hintcaseassets.shapeanimators;

import com.joanfuentes.hintcase.ShapeAnimator;
import com.joanfuentes.hintcase.Shape;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorProperty;
import ohos.agp.components.Component;

public class FadeOutShapeAnimator extends ShapeAnimator {

    public FadeOutShapeAnimator() {
        super();
    }

    public FadeOutShapeAnimator(int durationInMilliseconds) {
        super(durationInMilliseconds);
    }

    @Override
    public Animator getAnimator(Component component, Shape shape, final OnFinishListener onFinishListener) {
        shape.setMinimumValue();
        AnimatorProperty animator = new AnimatorProperty(component).alphaFrom(1).alpha(0);
        animator.setDuration(durationInMilliseconds);
        animator.setDelay(startDelayInMilliseconds);
        animator.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        if (onFinishListener != NO_CALLBACK) {
            animator.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {
                }

                @Override
                public void onStop(Animator animator) {
                }

                @Override
                public void onCancel(Animator animator) {
                }

                @Override
                public void onEnd(Animator animator) {
                    onFinishListener.onFinish();
                }

                @Override
                public void onPause(Animator animator) {
                }

                @Override
                public void onResume(Animator animator) {
                }
            });
        }
        return animator;
    }
}
