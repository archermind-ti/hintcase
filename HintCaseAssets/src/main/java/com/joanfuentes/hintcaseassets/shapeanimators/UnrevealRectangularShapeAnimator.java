package com.joanfuentes.hintcaseassets.shapeanimators;

import com.joanfuentes.hintcase.ShapeAnimator;
import com.joanfuentes.hintcase.RectangularShape;
import com.joanfuentes.hintcase.Shape;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

public class UnrevealRectangularShapeAnimator extends ShapeAnimator {

    AnimatorValue animator = new AnimatorValue();

    public UnrevealRectangularShapeAnimator() {
        super();
        animator = new AnimatorValue();
        animator.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        animator.setDelay(startDelayInMilliseconds);
        animator.setDuration(durationInMilliseconds);
    }

    public UnrevealRectangularShapeAnimator(int durationInMilliseconds) {
        super(durationInMilliseconds);
    }

    @Override
    public Animator getAnimator(final Component component, Shape shape, final OnFinishListener onFinishListener) {
        final RectangularShape rectangularShape = (RectangularShape) shape;
        final float distanceHeight = rectangularShape.getMaxHeight() - rectangularShape.getMinHeight();
        final float distanceWidth = rectangularShape.getMaxWidth() - rectangularShape.getMinWidth();
        animator.setValueUpdateListener((animator1, value) -> {
            rectangularShape.setCurrentHeight(rectangularShape.getMinHeight() + value * distanceHeight);
            rectangularShape.setCurrentWidth(rectangularShape.getMinWidth() + value * distanceWidth);
            if (rectangularShape.getCurrentHeight() == rectangularShape.getMaxHeight()) {
                rectangularShape.setMinimumValue();
                if (onFinishListener != null) {
                    onFinishListener.onFinish();
                }
            }
            component.invalidate();
        });
        return animator;
    }
}
