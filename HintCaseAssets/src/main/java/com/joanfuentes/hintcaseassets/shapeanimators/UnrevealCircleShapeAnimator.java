package com.joanfuentes.hintcaseassets.shapeanimators;

import com.joanfuentes.hintcase.ShapeAnimator;
import com.joanfuentes.hintcaseassets.shapes.CircularShape;
import com.joanfuentes.hintcase.Shape;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

public class UnrevealCircleShapeAnimator extends ShapeAnimator {

    AnimatorValue animator;

    public UnrevealCircleShapeAnimator() {
        super();
        animator = new AnimatorValue();
        animator.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        animator.setDelay(startDelayInMilliseconds);
        animator.setDuration(durationInMilliseconds);
    }

    public UnrevealCircleShapeAnimator(int durationInMilliseconds) {
        super(durationInMilliseconds);
    }

    @Override
    public Animator getAnimator(final Component component, Shape shape, final OnFinishListener onFinishListener) {
        final CircularShape circularShape = (CircularShape) shape;
        final float distance = circularShape.getMaxRadius() - circularShape.getMinRadius();
        animator.setValueUpdateListener((animator1, value) -> {
            circularShape.setCurrentRadius(circularShape.getMinRadius() + value * distance);
            if (circularShape.getCurrentRadius() == circularShape.getMaxRadius() && onFinishListener != null) {
                onFinishListener.onFinish();
            }
            component.invalidate();
        });
        return animator;
    }
}
