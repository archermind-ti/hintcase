package com.joanfuentes.hintcaseassets.shapeanimators;

import com.joanfuentes.hintcase.ShapeAnimator;
import com.joanfuentes.hintcase.RectangularShape;
import com.joanfuentes.hintcase.Shape;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.Component;

public class RevealRectangularShapeAnimator extends ShapeAnimator {

    AnimatorValue animator;

    public RevealRectangularShapeAnimator() {
        super();
        animator = new AnimatorValue();
        animator.setCurveType(Animator.CurveType.ACCELERATE_DECELERATE);
        animator.setDelay(startDelayInMilliseconds);
        animator.setDuration(durationInMilliseconds);
    }

    public RevealRectangularShapeAnimator(int durationInMilliseconds) {
        super(durationInMilliseconds);
    }

    @Override
    public Animator getAnimator(final Component component, Shape shape, final OnFinishListener onFinishListener) {
        final RectangularShape rectangularShape = (RectangularShape) shape;
        final float distanceHeight = rectangularShape.getMaxHeight() - rectangularShape.getMinHeight();
        final float distanceWidth = rectangularShape.getMaxWidth() - rectangularShape.getMinWidth();
        animator.setValueUpdateListener((animator1, value) -> {
            rectangularShape.setCurrentHeight(rectangularShape.getMaxHeight() - value * distanceHeight);
            rectangularShape.setCurrentWidth(rectangularShape.getMaxWidth() - value * distanceWidth);
            if (rectangularShape.getCurrentHeight() == rectangularShape.getMinHeight()) {
                rectangularShape.setMinimumValue();
                if (onFinishListener != null) {
                    onFinishListener.onFinish();
                }
            }
            component.invalidate();
        });
        return animator;
    }
}
