package com.joanfuentes.hintcaseexample;

import com.joanfuentes.hintcaseexample.slice.TargetHintAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class TargetHintAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(TargetHintAbilitySlice.class.getName());
    }
}
