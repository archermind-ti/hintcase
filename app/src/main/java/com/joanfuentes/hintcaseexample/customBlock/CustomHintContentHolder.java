package com.joanfuentes.hintcaseexample.customBlock;


import com.joanfuentes.hintcase.HintCase;
import com.joanfuentes.hintcase.utils.ComponentUtils;
import com.joanfuentes.hintcaseassets.hintcontentholders.HintContentHolder;
import com.joanfuentes.hintcaseexample.ResourceTable;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.utils.TextAlignment;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by joanFuentes on 26/05/16
 * <p>
 * Example of a custom HintContentHolder which includes an arrow and the necessary methods
 * for positioning the arrow based on the position of the hint.
 */
public class CustomHintContentHolder extends HintContentHolder {
    public static final int NO_IMAGE = -1;
    private static final int DEFAULT_ARROW_SIZE_IN_PX = 50;
    private static final int DEFAULT_SHADOW_SIZE_IN_PX = 0;
    public static final int DEFAULT_BORDER_SIZE_IN_PX = 3;
    public static final int DEFAULT_BORDER_COLOR = 0xff999999;

    private int borderColor = DEFAULT_BORDER_COLOR;
    private int backgroundColor;
    private int borderSize = DEFAULT_BORDER_SIZE_IN_PX;
    private Image imageComponent;
    private int imageResourceId;
    private String contentTitle;
    private String contentText;
    private int titleStyleId;
    private int textStyleId;
    private int marginLeft;
    private int marginTop;
    private int marginRight;
    private int marginBottom;
    private List<Integer> alignBlockRules;
    private List<Integer> alignArrowRules;
    private int contentTopMargin;
    private int contentBottomMargin;
    private int contentRightMargin;
    private int contentLeftMargin;
    private int contentTopPadding;
    private int contentBottomPadding;
    private int contentRightPadding;
    private int contentLeftPadding;
    private int alignment;
    private float xTranslationImage;
    private float yTranslationImage;
    private TriangleShapeComponent arrow;
    private HintCase hintCase;
    private ComponentContainer parent;
    private DirectionalLayout contentDirectionalLayout;
    private int arrowWidth;
    private int arrowHeight;
    private int shadowSize;
    private boolean useBorder;
    private TriangleShapeComponent.Direction arrowDirection;

    @Override
    public Component getComponent(Context context, final HintCase hintCase, ComponentContainer parent) {
        this.hintCase = hintCase;
        this.parent = parent;

        calculateDataToPutTheArrow(hintCase);
        setArrow(context);

        StackLayout.LayoutConfig stackLayoutConfigBlock = getParentLayoutConfig(
                ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT,
                alignment, marginLeft, marginTop, marginRight, marginBottom);

        DependentLayout fullBlockLayout = new DependentLayout(context);
        fullBlockLayout.setLayoutConfig(stackLayoutConfigBlock);

        DependentLayout.LayoutConfig dependentLayoutConfigDirectional =
                new DependentLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                        ComponentContainer.LayoutConfig.MATCH_CONTENT);
        for (int rule : alignBlockRules) {
            dependentLayoutConfigDirectional.addRule(rule);
        }
        dependentLayoutConfigDirectional.setMarginTop(contentTopMargin);
        dependentLayoutConfigDirectional.setMarginBottom(contentBottomMargin);
        dependentLayoutConfigDirectional.setMarginRight(contentRightMargin);
        dependentLayoutConfigDirectional.setMarginLeft(contentLeftMargin);
        contentDirectionalLayout = new DirectionalLayout(context);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setCornerRadius(ComponentUtils.vpToPixels(context, 10));
        shapeElement.setShape(ShapeElement.RECTANGLE);
        shapeElement.setRgbColor(RgbColor.fromArgbInt(backgroundColor));
        if (useBorder) {
            shapeElement.setStroke(borderSize, RgbColor.fromArgbInt(borderColor));
        }
        contentDirectionalLayout.setBackground(shapeElement);
        contentDirectionalLayout.setLayoutConfig(dependentLayoutConfigDirectional);
        contentDirectionalLayout.setAlignment(LayoutAlignment.CENTER);
        contentDirectionalLayout.setOrientation(DirectionalLayout.VERTICAL);
        if (contentTitle != null) {
            contentDirectionalLayout.addComponent(getTextTitle(context));
        }
        if (existImage()) {
            contentDirectionalLayout.addComponent(getImage(context, imageComponent, imageResourceId));
        }
        if (contentText != null) {
            contentDirectionalLayout.addComponent(getTextDescription(context));
        }
        if (contentDirectionalLayout.getChildCount() > 0) {
            contentDirectionalLayout.getComponentAt(contentDirectionalLayout.getChildCount() - 1).setMarginBottom(100 + contentBottomPadding);
        }
        int padding = borderSize + shadowSize;
        for (int i = 0; i < contentDirectionalLayout.getChildCount(); i++) {
            Component component = contentDirectionalLayout.getComponentAt(i);
            if (i == 0) {
                component.setMarginTop(padding + contentTopPadding);
            } else if (i == contentDirectionalLayout.getChildCount() - 1) {
                component.setMarginBottom(padding + contentBottomPadding);
            }
            component.setMarginsLeftAndRight(padding + contentLeftPadding, padding + contentRightPadding);
        }
        fullBlockLayout.addComponent(contentDirectionalLayout);
        fullBlockLayout.addComponent(arrow);
        return fullBlockLayout;
    }

    @Override
    public void onArrange() {
        calculateArrowTranslation();
        arrow.setLayoutRefreshedListener(component -> {
            arrow.setTranslationX(xTranslationImage);
            arrow.setTranslationY(yTranslationImage);
        });
        if ((hintCase.getBlockInfoPosition() == HintCase.HINT_BLOCK_POSITION_RIGHT
                || hintCase.getBlockInfoPosition() == HintCase.HINT_BLOCK_POSITION_LEFT)
                && arrow.getBottom() >= contentDirectionalLayout.getBottom()) {
            float translationY = arrow.getPivotY() + (arrow.getHeight() / 2f) - contentDirectionalLayout.getPivotY() - (contentDirectionalLayout.getHeight() / 2f);
            contentDirectionalLayout.setTranslationY(translationY);
        }
    }

    private void setArrow(Context context) {
        DependentLayout.LayoutConfig dependentLayoutConfigArrow =
                new DependentLayout.LayoutConfig(arrowWidth, arrowHeight);
        for (int rule : alignArrowRules) {
            dependentLayoutConfigArrow.addRule(rule);
        }
        arrow = new TriangleShapeComponent(context);
        arrow.setBackgroundColor(backgroundColor);
        if (useBorder) {
            arrow.setBorder(borderSize, borderColor);
        }
        arrow.setDirection(arrowDirection);
        arrow.setShadowSize(shadowSize);
        arrow.setLayoutConfig(dependentLayoutConfigArrow);
    }

    private void calculateArrowTranslation() {
        int textPosition = hintCase.getBlockInfoPosition();
        switch (textPosition) {
            case HintCase.HINT_BLOCK_POSITION_TOP:
            case HintCase.HINT_BLOCK_POSITION_BOTTOM:
                xTranslationImage = hintCase.getShape().getCenterX() - parent.getWidth() / 2f;
                yTranslationImage = 0f;
                break;
            case HintCase.HINT_BLOCK_POSITION_RIGHT:
            case HintCase.HINT_BLOCK_POSITION_LEFT:
                xTranslationImage = 0f;
                yTranslationImage = hintCase.getShape().getCenterY() - parent.getHeight() / 2f;
                break;
            default:
                xTranslationImage = 0;
                yTranslationImage = 0;
        }
    }

    private void calculateDataToPutTheArrow(HintCase hintCase) {
        int textPosition = hintCase.getBlockInfoPosition();
        alignArrowRules = new ArrayList<>();
        alignBlockRules = new ArrayList<>();

        switch (textPosition) {
            case HintCase.HINT_BLOCK_POSITION_TOP:
                alignBlockRules.add(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM);
                alignArrowRules.add(DependentLayout.LayoutConfig.HORIZONTAL_CENTER);
                alignArrowRules.add(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM);
                alignment = LayoutAlignment.BOTTOM;
                contentRightMargin = 0;
                contentLeftMargin = 0;
                contentTopMargin = 0;
                contentBottomMargin = arrowHeight - borderSize - shadowSize;
                arrowDirection = TriangleShapeComponent.Direction.DOWN;
                marginBottom = 0;
                break;
            case HintCase.HINT_BLOCK_POSITION_BOTTOM:
                alignBlockRules.add(DependentLayout.LayoutConfig.ALIGN_PARENT_TOP);
                alignArrowRules.add(DependentLayout.LayoutConfig.HORIZONTAL_CENTER);
                alignArrowRules.add(DependentLayout.LayoutConfig.ALIGN_PARENT_TOP);
                alignment = LayoutAlignment.TOP;
                contentRightMargin = 0;
                contentLeftMargin = 0;
                contentTopMargin = arrowHeight - borderSize - shadowSize;
                contentBottomMargin = 0;
                arrowDirection = TriangleShapeComponent.Direction.UP;
                marginTop = 0;
                break;
            case HintCase.HINT_BLOCK_POSITION_RIGHT:
                alignBlockRules.add(DependentLayout.LayoutConfig.ALIGN_PARENT_LEFT);
                alignArrowRules.add(DependentLayout.LayoutConfig.VERTICAL_CENTER);
                alignArrowRules.add(DependentLayout.LayoutConfig.ALIGN_PARENT_LEFT);
                alignment = LayoutAlignment.LEFT;
                contentRightMargin = 0;
                contentLeftMargin = arrowWidth;
                contentTopMargin = 0;
                contentBottomMargin = 0;
                arrowDirection = TriangleShapeComponent.Direction.LEFT;
                marginLeft = 0;
                break;
            case HintCase.HINT_BLOCK_POSITION_LEFT:
                alignBlockRules.add(DependentLayout.LayoutConfig.ALIGN_PARENT_RIGHT);
                alignArrowRules.add(DependentLayout.LayoutConfig.ALIGN_PARENT_RIGHT);
                alignArrowRules.add(DependentLayout.LayoutConfig.VERTICAL_CENTER);
                alignment = LayoutAlignment.RIGHT;
                contentRightMargin = arrowWidth;
                contentLeftMargin = 0;
                contentTopMargin = 0;
                contentBottomMargin = 0;
                arrowDirection = TriangleShapeComponent.Direction.RIGHT;
                marginRight = 0;
                break;
            default:
                alignBlockRules.add(DependentLayout.LayoutConfig.HORIZONTAL_CENTER);
                alignArrowRules.add(DependentLayout.LayoutConfig.CENTER_IN_PARENT);
                alignment = LayoutAlignment.CENTER;
                contentRightMargin = 0;
                contentLeftMargin = 0;
                contentTopMargin = 0;
                contentBottomMargin = 0;
                xTranslationImage = 0;
                yTranslationImage = 0;
        }
    }

    private Image getImage(Context context, Image image, int imageResourceId) {
        DirectionalLayout.LayoutConfig directionalLayoutConfigImage =
                new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                        ComponentContainer.LayoutConfig.MATCH_CONTENT, LayoutAlignment.HORIZONTAL_CENTER,
                        1.0f);
        directionalLayoutConfigImage.setMargins(0, 20, 0, 50);
        if (image == null) {
            image = new Image(context);
        }
        if (imageResourceId != CustomHintContentHolder.NO_IMAGE) {
            image.setPixelMap(imageResourceId);
        }
        image.setScaleMode(Image.ScaleMode.INSIDE);
        image.setLayoutConfig(directionalLayoutConfigImage);
        return image;
    }

    private Component getTextTitle(Context context) {
        DirectionalLayout.LayoutConfig directionalLayoutConfigTitle =
                new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT, ComponentContainer.LayoutConfig.MATCH_CONTENT);
        directionalLayoutConfigTitle.setMargins(0, 0, 0, 20);
        Text textTitle = new Text(context);
        textTitle.setLayoutConfig(directionalLayoutConfigTitle);
        textTitle.setTextAlignment(TextAlignment.HORIZONTAL_CENTER);
        textTitle.setText(contentTitle);
        ComponentUtils.applyStyle(context, textTitle, titleStyleId);
        return textTitle;
    }

    private Component getTextDescription(Context context) {
        DirectionalLayout.LayoutConfig directionalLayoutConfigText =
                new DirectionalLayout.LayoutConfig(ComponentContainer.LayoutConfig.MATCH_CONTENT,
                        ComponentContainer.LayoutConfig.MATCH_CONTENT);
        Text textDescription = new Text(context);
        textDescription.setLayoutConfig(directionalLayoutConfigText);
        textDescription.setTextAlignment(TextAlignment.HORIZONTAL_CENTER);
        textDescription.setText(contentText);
        textDescription.setMultipleLine(true);
        ComponentUtils.applyStyle(context, textDescription, textStyleId);
        return textDescription;
    }

    private boolean existImage() {
        return imageComponent != null ||
                imageResourceId != CustomHintContentHolder.NO_IMAGE;
    }

    public static class Builder {
        private final CustomHintContentHolder blockInfo;
        private final Context context;

        public Builder(Context context) {
            this.context = context;
            this.blockInfo = new CustomHintContentHolder();
            this.blockInfo.imageResourceId = NO_IMAGE;
            this.blockInfo.arrowWidth = DEFAULT_ARROW_SIZE_IN_PX;
            this.blockInfo.arrowHeight = DEFAULT_ARROW_SIZE_IN_PX;
            this.blockInfo.useBorder = true;
            try {
                this.blockInfo.shadowSize = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(ResourceTable.Integer_shadow).getInteger());
            } catch (IOException | NotExistException | WrongTypeException e) {
                e.printStackTrace();
                this.blockInfo.shadowSize = DEFAULT_SHADOW_SIZE_IN_PX;
            }
        }

        public Builder setBorder(int resourceId, int resId) {
            blockInfo.useBorder = true;
            blockInfo.borderColor = context.getColor(resId);
            try {
                blockInfo.borderSize = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(resourceId).getInteger());
            } catch (IOException | NotExistException | WrongTypeException e) {
                e.printStackTrace();
            }
            return this;
        }

        public Builder setBackgroundColorFromResource(int resId) {
            blockInfo.backgroundColor = context.getColor(resId);
            return this;
        }

        public Builder setBackgroundColor(int color) {
            blockInfo.backgroundColor = color;
            return this;
        }

        public Builder setImageResourceId(int resourceId) {
            blockInfo.imageResourceId = resourceId;
            return this;
        }

        public Builder setImage(Image image) {
            blockInfo.imageComponent = image;
            return this;
        }

        public Builder setContentTitle(String title) {
            blockInfo.contentTitle = title;
            return this;
        }

        public Builder setContentTitle(int resId) {
            blockInfo.contentTitle = context.getString(resId);
            return this;
        }

        public Builder setTitleStyle(int resId) {
            blockInfo.titleStyleId = resId;
            return this;
        }

        public Builder setContentText(String text) {
            blockInfo.contentText = text;
            return this;
        }

        public Builder setContentText(int resId) {
            blockInfo.contentText = context.getString(resId);
            return this;
        }

        public Builder setContentStyle(int resId) {
            blockInfo.textStyleId = resId;
            return this;
        }

        public Builder setMargin(int left, int top, int right, int bottom) {
            blockInfo.marginLeft = left;
            blockInfo.marginTop = top;
            blockInfo.marginRight = right;
            blockInfo.marginBottom = bottom;
            return this;
        }

        public Builder setMarginByResourcesId(int left, int top, int right, int bottom) {
            try {
                blockInfo.marginLeft = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(left).getInteger());
                blockInfo.marginTop = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(top).getInteger());
                blockInfo.marginRight = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(right).getInteger());
                blockInfo.marginBottom = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(bottom).getInteger());
            } catch (IOException | NotExistException | WrongTypeException e) {
                e.printStackTrace();
            }
            return this;
        }

        public Builder setContentPadding(int left, int top, int right, int bottom) {
            blockInfo.contentLeftPadding = left;
            blockInfo.contentTopPadding = top;
            blockInfo.contentRightPadding = right;
            blockInfo.contentBottomPadding = bottom;
            return this;
        }

        public Builder setContentPaddingByResourcesId(int left, int top, int right, int bottom) {
            try {
                blockInfo.contentLeftPadding = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(left).getInteger());
                blockInfo.contentTopPadding = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(top).getInteger());
                blockInfo.contentRightPadding = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(right).getInteger());
                blockInfo.contentBottomPadding = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(bottom).getInteger());
            } catch (IOException | NotExistException | WrongTypeException e) {
                e.printStackTrace();
            }
            return this;
        }

        public Builder setArrowSize(int widthResId, int heightResId) {
            try {
                blockInfo.arrowWidth = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(widthResId).getInteger());
                blockInfo.arrowHeight = ComponentUtils.vpToPixels(context, context.getResourceManager().getElement(heightResId).getInteger());
            } catch (IOException | NotExistException | WrongTypeException e) {
                e.printStackTrace();
            }
            return this;
        }

        public CustomHintContentHolder build() {
            return blockInfo;
        }
    }
}
