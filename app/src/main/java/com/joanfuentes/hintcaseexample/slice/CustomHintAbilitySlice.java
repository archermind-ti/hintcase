package com.joanfuentes.hintcaseexample.slice;

import com.joanfuentes.hintcase.HintCase;
import com.joanfuentes.hintcase.utils.ComponentUtils;
import com.joanfuentes.hintcaseassets.contentholderanimators.FadeInContentHolderAnimator;
import com.joanfuentes.hintcaseassets.contentholderanimators.FadeOutContentHolderAnimator;
import com.joanfuentes.hintcaseassets.contentholderanimators.SlideInFromRightContentHolderAnimator;
import com.joanfuentes.hintcaseassets.contentholderanimators.SlideOutFromRightContentHolderAnimator;
import com.joanfuentes.hintcaseassets.extracontentholders.SimpleButtonContentHolder;
import com.joanfuentes.hintcaseexample.ResourceTable;
import com.joanfuentes.hintcaseexample.customBlock.CustomHintContentHolder;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.agp.window.service.WindowManager;

public class CustomHintAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        super.setUIContent(ResourceTable.Layout_ability_custom_hint);
        setComponents();
    }

    private void setComponents() {
        Button buttonUp = (Button) findComponentById(ResourceTable.Id_button_up);
        if(buttonUp != null) {
            buttonUp.setClickedListener(this::showHint);
        }

        Button buttonDown = (Button) findComponentById(ResourceTable.Id_button_down);
        if(buttonDown != null) {
            buttonDown.setClickedListener(this::showHint);
        }

        Button buttonRight = (Button) findComponentById(ResourceTable.Id_button_right);
        if(buttonRight != null) {
            buttonRight.setClickedListener(this::showHint);
        }

        Button buttonLeft = (Button) findComponentById(ResourceTable.Id_button_left);
        if(buttonLeft != null) {
            buttonLeft.setClickedListener(this::showHint);
        }
    }

    private void showHint(final Component component) {
        SimpleButtonContentHolder okBlock = new SimpleButtonContentHolder.Builder(component.getContext())
                .setWidth(ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setHeight(ComponentContainer.LayoutConfig.MATCH_CONTENT)
                .setRules(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM, DependentLayout.LayoutConfig.ALIGN_PARENT_RIGHT)
                .setButtonText("OK")
                .setCloseHintCaseOnClick(true)
                .setButtonStyle(ResourceTable.Pattern_buttonNice)
                .build();
        CustomHintContentHolder blockInfo = new CustomHintContentHolder.Builder(component.getContext())
                .setContentTitle("Custom Hint Content Holder!")
                .setContentText("This hint was done with a custom hint content holder and it only can be closed clicking over the blue OK button")
                .setBorder(ResourceTable.Integer_bubble_border, ResourceTable.Color_holo_blue_dark)
                .setArrowSize(ResourceTable.Integer_arrow_width, ResourceTable.Integer_arrow_height)
                .setBackgroundColor(Color.WHITE.getValue())
                .setTitleStyle(ResourceTable.Pattern_title)
                .setMarginByResourcesId(ResourceTable.Integer_activity_vertical_margin,
                        ResourceTable.Integer_activity_horizontal_margin,
                        ResourceTable.Integer_activity_vertical_margin,
                        ResourceTable.Integer_activity_horizontal_margin)
                .setContentPaddingByResourcesId(ResourceTable.Integer_small_margin,
                        ResourceTable.Integer_small_margin,
                        ResourceTable.Integer_small_margin,
                        ResourceTable.Integer_small_margin)
                .setContentStyle(ResourceTable.Pattern_content_dark)
                .build();
        new HintCase(ComponentUtils.getRootComponent(component))
                .setTarget(component, ResourceTable.Integer_zero_margin)
                .setBackgroundColor(HintCase.BACKGROUND_COLOR_TRANSPARENT)
                .setHintBlock(blockInfo, new FadeInContentHolderAnimator(),
                        new FadeOutContentHolderAnimator())
                .setExtraBlock(okBlock, new SlideInFromRightContentHolderAnimator(),
                        new SlideOutFromRightContentHolderAnimator())
                .setCloseOnTouchComponent(false)
                .show();
    }

}
