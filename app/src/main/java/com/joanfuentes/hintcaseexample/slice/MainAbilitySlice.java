package com.joanfuentes.hintcaseexample.slice;

import com.joanfuentes.hintcase.HintCase;
import com.joanfuentes.hintcaseexample.CustomHintAbility;
import com.joanfuentes.hintcaseexample.FullHintAbility;
import com.joanfuentes.hintcaseexample.ResourceTable;
import com.joanfuentes.hintcaseexample.TargetHintAbility;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Button;
import ohos.agp.window.service.WindowManager;

public class MainAbilitySlice extends AbilitySlice {
    private final Intent intent = new Intent();
    private final Operation fullOperation = new Intent.OperationBuilder().withDeviceId("")
            .withBundleName(HintCase.class.getPackage().getName())
            .withAbilityName(FullHintAbility.class.getName()).build();
    private final Operation targetOperation = new Intent.OperationBuilder().withDeviceId("")
            .withBundleName(HintCase.class.getPackage().getName())
            .withAbilityName(TargetHintAbility.class.getName()).build();
    private final Operation customOperation = new Intent.OperationBuilder().withDeviceId("")
            .withBundleName(HintCase.class.getPackage().getName())
            .withAbilityName(CustomHintAbility.class.getName()).build();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_NAVIGATION);
        super.setUIContent(ResourceTable.Layout_ability_main);
        setComponents();
    }

    private void setComponents() {

        Button buttonFullScreen = (Button) findComponentById(ResourceTable.Id_button_full_screen_hints);
        if (buttonFullScreen != null) {
            buttonFullScreen.setClickedListener(component -> {
                intent.setOperation(fullOperation);
                startAbility(intent);
            });
        }

        Button buttonTargetHints = (Button) findComponentById(ResourceTable.Id_button_target_hints);
        if (buttonTargetHints != null) {
            buttonTargetHints.setClickedListener(component -> {
                intent.setOperation(targetOperation);
                startAbility(intent);
            });
        }

        Button buttonCustomHints = (Button) findComponentById(ResourceTable.Id_button_custom_hints);
        if (buttonCustomHints != null) {
            buttonCustomHints.setClickedListener(component -> {
                intent.setOperation(customOperation);
                startAbility(intent);
            });
        }
    }
}
