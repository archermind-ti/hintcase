package com.joanfuentes.hintcaseexample.slice;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.resource.gif.drawableability.DraweeView;
import com.joanfuentes.hintcase.HintCase;
import com.joanfuentes.hintcase.RectangularShape;
import com.joanfuentes.hintcase.ShapeAnimator;
import com.joanfuentes.hintcase.utils.ComponentUtils;
import com.joanfuentes.hintcaseassets.contentholderanimators.FadeInContentHolderAnimator;
import com.joanfuentes.hintcaseassets.contentholderanimators.FadeOutContentHolderAnimator;
import com.joanfuentes.hintcaseassets.contentholderanimators.SlideInFromRightContentHolderAnimator;
import com.joanfuentes.hintcaseassets.contentholderanimators.SlideOutFromRightContentHolderAnimator;
import com.joanfuentes.hintcaseassets.hintcontentholders.SimpleHintContentHolder;
import com.joanfuentes.hintcaseassets.shapeanimators.*;
import com.joanfuentes.hintcaseassets.shapes.CircularShape;
import com.joanfuentes.hintcaseexample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.*;
import ohos.agp.window.service.WindowManager;

public class FullHintAbilitySlice extends AbilitySlice {
    private static final Component  NO_COMPONENT_AS_TARGET = null;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_NAVIGATION);
        super.setUIContent(ResourceTable.Layout_ability_full_hint);
        setComponents();
    }

    private void setComponents() {
        Button buttonExample1 = (Button) findComponentById(ResourceTable.Id_button_example_1);
        if(buttonExample1 != null) {
            buttonExample1.setClickedListener(component -> {
                Image animatedImageComponent = getGifLoadedUsingGlide();
                SimpleHintContentHolder blockInfo =
                        getSimpleHintContentHolder(component, animatedImageComponent);
                new HintCase(ComponentUtils.getRootComponent(component))
                        .setBackgroundColor(getColor(ResourceTable.Color_colorPrimary))
                        .setShapeAnimators(new FadeInShapeAnimator(), new FadeOutShapeAnimator())
                        .setHintBlock(blockInfo)
                        .show();
            });
        }

        Button buttonExample2 = (Button) findComponentById(ResourceTable.Id_button_example_2);
        if(buttonExample2 != null) {
            buttonExample2.setClickedListener(component -> {
                Image animatedImageComponent = getGifLoadedUsingGlide();
                SimpleHintContentHolder blockInfo =
                        getSimpleHintContentHolder(component, animatedImageComponent);
                new HintCase(ComponentUtils.getRootComponent(component))
                        .setBackgroundColor(getColor(ResourceTable.Color_colorPrimary))
                        .setTarget(NO_COMPONENT_AS_TARGET, new CircularShape())
                        .setShapeAnimators(new RevealCircleShapeAnimator(),
                                new UnrevealCircleShapeAnimator())
                        .setHintBlock(blockInfo)
                        .show();

            });
        }

        Button buttonExample3 = (Button) findComponentById(ResourceTable.Id_button_example_3);
        if(buttonExample3 != null) {
            buttonExample3.setClickedListener(component -> {
                Image animatedImageComponent = getGifLoadedUsingGlide();
                SimpleHintContentHolder blockInfo =
                        getSimpleHintContentHolder(component, animatedImageComponent);
                new HintCase(ComponentUtils.getRootComponent(component))
                        .setBackgroundColor(getColor(ResourceTable.Color_colorPrimary))
                        .setTarget(NO_COMPONENT_AS_TARGET, new RectangularShape())
                        .setShapeAnimators(new RevealRectangularShapeAnimator(),
                                new UnrevealRectangularShapeAnimator())
                        .setHintBlock(blockInfo)
                        .show();
            });
        }

        Button buttonExample4 = (Button) findComponentById(ResourceTable.Id_button_example_4);
        if(buttonExample4 != null) {
            buttonExample4.setClickedListener(component -> {
                Image animatedImageComponent = getGifLoadedUsingGlide();
                SimpleHintContentHolder blockInfo =
                        getSimpleHintContentHolder(component, animatedImageComponent);
                new HintCase(ComponentUtils.getRootComponent(component))
                        .setBackgroundColor(getColor(ResourceTable.Color_colorPrimary))
                        .setShapeAnimators(new FadeInShapeAnimator(), new FadeOutShapeAnimator())
                        .setHintBlock(blockInfo, new FadeInContentHolderAnimator(),
                                new FadeOutContentHolderAnimator())
                        .show();
            });
        }

        Button buttonExample5 = (Button) findComponentById(ResourceTable.Id_button_example_5);
        if(buttonExample5 != null) {
            buttonExample5.setClickedListener(component -> {
                Image animatedImageComponent = getGifLoadedUsingGlide();
                SimpleHintContentHolder blockInfo =
                        getSimpleHintContentHolder(component, animatedImageComponent);
                new HintCase(ComponentUtils.getRootComponent(component))
                        .setBackgroundColor(getColor(ResourceTable.Color_colorPrimary))
                        .setTarget(NO_COMPONENT_AS_TARGET, new CircularShape())
                        .setShapeAnimators(new RevealCircleShapeAnimator(),
                                new UnrevealCircleShapeAnimator())
                        .setHintBlock(blockInfo, new FadeInContentHolderAnimator(),
                                new FadeOutContentHolderAnimator())
                        .show();

            });
        }

        Button buttonExample6 = (Button) findComponentById(ResourceTable.Id_button_example_6);
        if(buttonExample6 != null) {
            buttonExample6.setClickedListener(component -> {
                Image animatedImageComponent = getGifLoadedUsingGlide();
                SimpleHintContentHolder blockInfo =
                        getSimpleHintContentHolder(component, animatedImageComponent);
                new HintCase(ComponentUtils.getRootComponent(component))
                        .setBackgroundColor(getColor(ResourceTable.Color_colorPrimary))
                        .setTarget(NO_COMPONENT_AS_TARGET, new CircularShape())
                        .setShapeAnimators(new RevealCircleShapeAnimator(),
                                new UnrevealCircleShapeAnimator())
                        .setHintBlock(blockInfo, new SlideInFromRightContentHolderAnimator(),
                                new SlideOutFromRightContentHolderAnimator())
                        .show();

            });
        }

        Button buttonExample7 = (Button) findComponentById(ResourceTable.Id_button_example_7);
        if(buttonExample7 != null) {
            buttonExample7.setClickedListener(this::launchFirstHint);
        }
    }

    private void launchFirstHint(final Component component) {
        SimpleHintContentHolder blockInfo = new SimpleHintContentHolder.Builder(component.getContext())
                .setContentTitle("Attention!")
                .setContentText("This is your first advice. Please, be careful")
                .setTitleStyle(ResourceTable.Pattern_title)
                .setContentStyle(ResourceTable.Pattern_content)
                .build();
        new HintCase(ComponentUtils.getRootComponent(component))
                .setTarget(NO_COMPONENT_AS_TARGET, new CircularShape(), HintCase.TARGET_IS_NOT_CLICKABLE)
                .setBackgroundColor(getColor(ResourceTable.Color_colorPrimary))
                .setShapeAnimators(new RevealCircleShapeAnimator(), ShapeAnimator.NO_ANIMATOR)
                .setHintBlock(blockInfo, new FadeInContentHolderAnimator(), new SlideOutFromRightContentHolderAnimator())
                .setOnClosedListener(() -> launchSecondHint(component))
                .show();
    }

    private void launchSecondHint(Component component) {
        SimpleHintContentHolder blockInfo = new SimpleHintContentHolder.Builder(component.getContext())
                .setContentTitle("Attention again!")
                .setContentText("Are you really reading these messages?")
                .setTitleStyle(ResourceTable.Pattern_title)
                .setContentStyle(ResourceTable.Pattern_content)
                .build();
        new HintCase(ComponentUtils.getRootComponent(component))
                .setTarget(NO_COMPONENT_AS_TARGET, new CircularShape())
                .setBackgroundColor(getColor(ResourceTable.Color_colorPrimary))
                .setShapeAnimators(ShapeAnimator.NO_ANIMATOR, new UnrevealCircleShapeAnimator())
                .setHintBlock(blockInfo, new SlideInFromRightContentHolderAnimator())
                .show();
    }

    private Image getGifLoadedUsingGlide() {
        DraweeView animatedImageComponent = new DraweeView(this);
        animatedImageComponent.setPixelMap(ResourceTable.Media_animated_image);
        Glide.with(getContext())
                .asGif()
                .load(ResourceTable.Media_animated_image)
                .into(animatedImageComponent);
        return animatedImageComponent;
    }

    private SimpleHintContentHolder getSimpleHintContentHolder(Component component, Image animatedImageComponent) {
        return new SimpleHintContentHolder.Builder(component.getContext())
                .setContentTitle("The truth behind the veil")
                .setContentText("True story")
                .setImage(animatedImageComponent)
                .setTitleStyle(ResourceTable.Pattern_title)
                .setContentStyle(ResourceTable.Pattern_content)
                .setMarginByResourcesId(ResourceTable.Integer_activity_horizontal_margin,
                        ResourceTable.Integer_activity_vertical_margin, ResourceTable.Integer_activity_horizontal_margin,
                        ResourceTable.Integer_activity_vertical_margin)
                .build();
    }
}
