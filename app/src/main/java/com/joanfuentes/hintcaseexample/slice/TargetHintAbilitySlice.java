package com.joanfuentes.hintcaseexample.slice;

import com.joanfuentes.hintcase.HintCase;
import com.joanfuentes.hintcase.utils.ComponentUtils;
import com.joanfuentes.hintcaseassets.contentholderanimators.FadeInContentHolderAnimator;
import com.joanfuentes.hintcaseassets.hintcontentholders.SimpleHintContentHolder;
import com.joanfuentes.hintcaseassets.shapeanimators.RevealCircleShapeAnimator;
import com.joanfuentes.hintcaseassets.shapeanimators.RevealRectangularShapeAnimator;
import com.joanfuentes.hintcaseassets.shapeanimators.UnrevealCircleShapeAnimator;
import com.joanfuentes.hintcaseassets.shapeanimators.UnrevealRectangularShapeAnimator;
import com.joanfuentes.hintcaseassets.shapes.CircularShape;
import com.joanfuentes.hintcaseexample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Switch;
import ohos.agp.utils.LayoutAlignment;
import ohos.agp.window.dialog.ToastDialog;
import ohos.agp.window.service.WindowManager;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;


public class TargetHintAbilitySlice extends AbilitySlice {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_STATUS);
        getWindow().addFlags(WindowManager.LayoutConfig.MARK_TRANSLUCENT_NAVIGATION);
        super.setUIContent(ResourceTable.Layout_ability_target_hint);
        configureTitleBar();
        setComponents();
        launchAutomaticHint();
    }

    private void configureTitleBar() {
        Button buttonLeft = (Button) findComponentById(ResourceTable.Id_title_bar_left);
        if (buttonLeft != null) {
            buttonLeft.setClickedListener(component -> terminate());
        }
    }

    private void launchAutomaticHint() {
        new EventHandler(EventRunner.current()).postTask(() -> {
            Component actionSearchComponent = findComponentById(ResourceTable.Id_title_bar_right);
            if (actionSearchComponent != null) {
                SimpleHintContentHolder blockInfo =
                        new SimpleHintContentHolder.Builder(actionSearchComponent.getContext())
                                .setContentTitle("Search")
                                .setContentText("This is an automatic example of a hint over a toolbar item")
                                .setTitleStyle(ResourceTable.Pattern_title)
                                .setContentStyle(ResourceTable.Pattern_content)
                                .setMarginByResourcesId(ResourceTable.Integer_activity_horizontal_margin,
                                        ResourceTable.Integer_activity_vertical_margin,
                                        ResourceTable.Integer_activity_horizontal_margin,
                                        ResourceTable.Integer_activity_vertical_margin)
                                .build();
                new HintCase(ComponentUtils.getRootComponent(actionSearchComponent))
                        .setTarget(actionSearchComponent, new CircularShape())
                        .setShapeAnimators(new RevealCircleShapeAnimator(),
                                new UnrevealCircleShapeAnimator())
                        .setHintBlock(blockInfo)
                        .show();
            }
        }, 500);
    }

    private void setComponents() {
        Button buttonExample1 = (Button) findComponentById(ResourceTable.Id_button_example_1);
        if (buttonExample1 != null) {
            buttonExample1.setClickedListener(component -> {
                SimpleHintContentHolder blockInfo = new SimpleHintContentHolder.Builder(component.getContext())
                        .setContentTitle("Attention!")
                        .setContentText("This is a hint related with a button.")
                        .setTitleStyle(ResourceTable.Pattern_title)
                        .setContentStyle(ResourceTable.Pattern_content)
                        .build();
                new HintCase(ComponentUtils.getRootComponent(component))
                        .setTarget(findComponentById(ResourceTable.Id_button), HintCase.TARGET_IS_NOT_CLICKABLE)
                        .setBackgroundColorByResourceId(ResourceTable.Color_colorPrimary)
                        .setShapeAnimators(new RevealRectangularShapeAnimator(), new UnrevealRectangularShapeAnimator())
                        .setHintBlock(blockInfo, new FadeInContentHolderAnimator())
                        .show();
            });
        }

        Button buttonExample2 = (Button) findComponentById(ResourceTable.Id_button_example_2);
        if (buttonExample2 != null) {
            buttonExample2.setClickedListener(component -> {
                SimpleHintContentHolder blockInfo = new SimpleHintContentHolder.Builder(component.getContext())
                        .setContentTitle("Attention!")
                        .setContentText("This is a hint related with a text.. Please, be careful")
                        .setTitleStyle(ResourceTable.Pattern_title)
                        .setContentStyle(ResourceTable.Pattern_content)
                        .build();
                new HintCase(ComponentUtils.getRootComponent(component))
                        .setTarget(findComponentById(ResourceTable.Id_text), new CircularShape(), HintCase.TARGET_IS_NOT_CLICKABLE)
                        .setBackgroundColorByResourceId(ResourceTable.Color_colorPrimary)
                        .setShapeAnimators(new RevealCircleShapeAnimator(), new UnrevealCircleShapeAnimator())
                        .setHintBlock(blockInfo, new FadeInContentHolderAnimator())
                        .show();
            });
        }

        Switch switchButton = (Switch) findComponentById(ResourceTable.Id_switch_button);
        if (switchButton != null) {
            switchButton.setCheckedStateChangedListener((absButton, b) -> new ToastDialog(getContext()).setText("Switch was changed").show());
        }

        Button buttonExample3 = (Button) findComponentById(ResourceTable.Id_button_example_3);
        if (buttonExample3 != null) {
            buttonExample3.setClickedListener(component -> {
                SimpleHintContentHolder blockInfo = new SimpleHintContentHolder.Builder(component.getContext())
                        .setContentTitle("Activate your powers!")
                        .setContentText("you have the full control over your power. On to be a Hero, Off to be a looser.")
                        .setTitleStyle(ResourceTable.Pattern_title_light)
                        .setContentStyle(ResourceTable.Pattern_content_light)
                        .setAlignment(LayoutAlignment.CENTER)
                                .setMarginByResourcesId(ResourceTable.Integer_activity_horizontal_margin,
                                        ResourceTable.Integer_activity_vertical_margin,
                                        ResourceTable.Integer_activity_horizontal_margin,
                                        ResourceTable.Integer_activity_vertical_margin)
                        .build();
                new HintCase(ComponentUtils.getRootComponent(component))
                        .setTarget(findComponentById(ResourceTable.Id_switch_button), HintCase.TARGET_IS_CLICKABLE)
                        .setBackgroundColorByResourceId(ResourceTable.Color_holo_blue_dark)
                        .setShapeAnimators(new RevealRectangularShapeAnimator(),
                                new UnrevealRectangularShapeAnimator())
                        .setHintBlock(blockInfo, new FadeInContentHolderAnimator())
                        .show();
            });
        }

        Button buttonExample4 = (Button) findComponentById(ResourceTable.Id_button_example_4);
        if (buttonExample4 != null) {
            buttonExample4.setClickedListener(component -> {
                SimpleHintContentHolder blockInfo = new SimpleHintContentHolder.Builder(component.getContext())
                        .setContentTitle("Attention!")
                        .setContentText("This is a hint related with a radio button.")
                        .setTitleStyle(ResourceTable.Pattern_title)
                        .setContentStyle(ResourceTable.Pattern_content)
                        .setAlignment(LayoutAlignment.CENTER)
                                .setMarginByResourcesId(ResourceTable.Integer_activity_horizontal_margin,
                                        ResourceTable.Integer_activity_vertical_margin,
                                        ResourceTable.Integer_activity_horizontal_margin,
                                        ResourceTable.Integer_activity_vertical_margin)
                        .build();
                new HintCase(ComponentUtils.getRootComponent(component))
                        .setTarget(findComponentById(ResourceTable.Id_radio_button), HintCase.TARGET_IS_CLICKABLE)
                        .setBackgroundColor(0xCC000000)
                        .setShapeAnimators(new RevealRectangularShapeAnimator(), new UnrevealRectangularShapeAnimator())
                        .setHintBlock(blockInfo, new FadeInContentHolderAnimator())
                        .show();
            });
        }

        Button buttonExample5 = (Button) findComponentById(ResourceTable.Id_button_example_5);
        if (buttonExample5 != null) {
            buttonExample5.setClickedListener(component -> {
                SimpleHintContentHolder blockInfo = new SimpleHintContentHolder.Builder(component.getContext())
                        .setContentTitle("FAB button power!")
                        .setContentText("The FAB button is gonna help you with the main action of every screen.")
                        .setTitleStyle(ResourceTable.Pattern_title)
                        .setContentStyle(ResourceTable.Pattern_content)
                        .build();
                new HintCase(ComponentUtils.getRootComponent(component))
                        .setTarget(findComponentById(ResourceTable.Id_fab), new CircularShape())
                        .setShapeAnimators(new RevealCircleShapeAnimator(),
                                new UnrevealCircleShapeAnimator())
                        .setHintBlock(blockInfo)
                        .show();
            });
        }

        Button buttonExample6 = (Button) findComponentById(ResourceTable.Id_button_example_6);
        if (buttonExample6 != null) {
            buttonExample6.setClickedListener(component -> {
                Component actionCameraComponent = findComponentById(ResourceTable.Id_title_bar_right_two);
                if (actionCameraComponent != null) {
                    actionCameraComponent.setClickedListener(camera -> new ToastDialog(getContext()).setText("Camera was clicked").show());
                    SimpleHintContentHolder blockInfo =
                            new SimpleHintContentHolder.Builder(actionCameraComponent.getContext())
                                    .setContentTitle("Camera icon!")
                                    .setContentText("This is an example of a hint over a toolbar item")
                                    .setTitleStyle(ResourceTable.Pattern_title)
                                    .setContentStyle(ResourceTable.Pattern_content)
                                .setMarginByResourcesId(ResourceTable.Integer_activity_horizontal_margin,
                                        ResourceTable.Integer_activity_vertical_margin,
                                        ResourceTable.Integer_activity_horizontal_margin,
                                        ResourceTable.Integer_activity_vertical_margin)
                                    .build();
                    new HintCase(ComponentUtils.getRootComponent(component))
                            .setTarget(actionCameraComponent, new CircularShape(), HintCase.TARGET_IS_CLICKABLE)
                            .setShapeAnimators(new RevealCircleShapeAnimator(),
                                    new UnrevealCircleShapeAnimator())
                            .setHintBlock(blockInfo)
                            .show();
                }
            });
        }
    }

}
