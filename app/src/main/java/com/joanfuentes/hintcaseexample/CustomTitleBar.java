package com.joanfuentes.hintcaseexample;

import ohos.agp.components.*;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 * Created by 夏德旺 on 2021/3/4 10:01
 */
public class CustomTitleBar extends ComponentContainer {

    private static final HiLogLabel LABEL = new HiLogLabel(HiLog.DEBUG, 0, "TAG");

    public CustomTitleBar(Context context) {
        super(context);
    }

    public CustomTitleBar(Context context, AttrSet attrSet) {
        super(context, attrSet);
        //动态加载layout
        Component component = LayoutScatter.getInstance(context).parse(ResourceTable.Layout_layout_titlebar, null, false);
        Button leftBtn = (Button) component.findComponentById(ResourceTable.Id_title_bar_left);
        Text titleText = (Text) component.findComponentById(ResourceTable.Id_titleText);
        Button rightBtn = (Button) component.findComponentById(ResourceTable.Id_title_bar_right);
        Button rightBtnTwo = (Button) component.findComponentById(ResourceTable.Id_title_bar_right_two);
        //添加layout到父组件
        addComponent(component);
        //处理TitleBar背景色
        if(attrSet.getAttr("bg_color").isPresent()){
            component.setBackground(attrSet.getAttr("bg_color").get().getElement());
        }else{
            HiLog.error(LABEL,"attr bg_color is not present");
            component.setBackground(getBackgroundElement());
        }

        //处理标题文字
        if(attrSet.getAttr("title_text").isPresent()){
            titleText.setText(attrSet.getAttr("title_text").get().getStringValue());
        }else {
            HiLog.error(LABEL,"attr title_text is not present");
            titleText.setText("");
        }

        //处理标题大小
        if(attrSet.getAttr("title_size").isPresent()){
            titleText.setTextSize(attrSet.getAttr("title_size").get().getIntegerValue(), Text.TextSizeType.FP);
        }else {
            HiLog.error(LABEL,"attr title_size is not present");
        }
        //处理标题颜色
        if(attrSet.getAttr("title_color").isPresent()){
            titleText.setTextColor(attrSet.getAttr("title_color").get().getColorValue());
        }else{
            HiLog.error(LABEL,"attr title_color is not exist");
            titleText.setTextColor(Color.WHITE);
        }

        //处理左边按钮
        //获取是否要显示左边按钮
        if(attrSet.getAttr("left_button_visible").isPresent()){
            if(attrSet.getAttr("left_button_visible").get().getBoolValue()){
                leftBtn.setVisibility(VISIBLE);
            }else{
                leftBtn.setVisibility(HIDE);
            }
        }else{
            //默认情况显示
            HiLog.error(LABEL,"attr right_button_visible is not exist");
            leftBtn.setVisibility(VISIBLE);
        }
        //处理左侧按钮的图标
        if(attrSet.getAttr("left_button_icon").isPresent()){
            leftBtn.setBackground(attrSet.getAttr("left_button_icon").get().getElement());
        }else{
            HiLog.error(LABEL,"attr left_button_icon is not exist");
        }
        //处理左侧按钮的文本
        if(attrSet.getAttr("left_button_text").isPresent()){
            leftBtn.setText(attrSet.getAttr("left_button_text").get().getStringValue());
        }else{
            HiLog.error(LABEL,"attr left_button_text is not exist");
        }
        //处理左侧按钮的文本颜色
        if(attrSet.getAttr("left_button_text_color").isPresent()){
            leftBtn.setTextColor(attrSet.getAttr("left_button_text_color").get().getColorValue());
        }else{
            HiLog.error(LABEL,"attr left_button_text_color is not exist");
            leftBtn.setTextColor(Color.WHITE);
        }
        //处理左侧按钮的文本大小
        if(attrSet.getAttr("left_button_text_size").isPresent()){
            leftBtn.setTextSize(attrSet.getAttr("left_button_text_size").get().getIntegerValue(),Text.TextSizeType.FP);
        }else{
            HiLog.error(LABEL,"attr left_button_text_size is not exist");
        }

        //处理右边按钮
        //获取是否要显示右边按钮
        if(attrSet.getAttr("right_button_visible").isPresent()){
            if(attrSet.getAttr("right_button_visible").get().getBoolValue()){
                rightBtn.setVisibility(VISIBLE);
            }else{
                rightBtn.setVisibility(HIDE);
            }
        }else{
            //默认情况显示
            HiLog.error(LABEL,"attr right_button_visible is not exist");
            rightBtn.setVisibility(VISIBLE);
        }

        //处理右侧按钮的图标
        if(attrSet.getAttr("right_button_icon").isPresent()){
            rightBtn.setBackground(attrSet.getAttr("right_button_icon").get().getElement());
        }else{
            HiLog.error(LABEL,"attr right_button_icon is not exist");
        }
        //处理右侧按钮的文本
        if(attrSet.getAttr("right_button_text").isPresent()){
            rightBtn.setText(attrSet.getAttr("right_button_text").get().getStringValue());
        }else{
            HiLog.error(LABEL,"attr right_button_text is not exist");
        }
        //处理右侧按钮的文本颜色
        if(attrSet.getAttr("right_button_text_color").isPresent()){
            rightBtn.setTextColor(attrSet.getAttr("right_button_text_color").get().getColorValue());
        }else{
            HiLog.error(LABEL,"attr right_button_text_color is not exist");
        }
        //处理右侧按钮的文本大小
        if(attrSet.getAttr("right_button_text_size").isPresent()){
            rightBtn.setTextSize(attrSet.getAttr("right_button_text_size").get().getIntegerValue(),Text.TextSizeType.FP);
        }else{
            HiLog.error(LABEL,"attr right_button_text_size is not exist");
        }

        //处理右边按钮 TWO
        //获取是否要显示右边按钮
        if(attrSet.getAttr("right_button_two_visible").isPresent()){
            if(attrSet.getAttr("right_button_two_visible").get().getBoolValue()){
                rightBtnTwo.setVisibility(VISIBLE);
            }else{
                rightBtnTwo.setVisibility(HIDE);
            }
        }else{
            //默认情况显示
            HiLog.error(LABEL,"attr right_button_two_visible is not exist");
            rightBtnTwo.setVisibility(VISIBLE);
        }

        //处理右侧按钮的图标
        if(attrSet.getAttr("right_button_two_icon").isPresent()){
            rightBtnTwo.setBackground(attrSet.getAttr("right_button_two_icon").get().getElement());
        }else{
            HiLog.error(LABEL,"attr right_button_two_icon is not exist");
        }
        //处理右侧按钮的文本
        if(attrSet.getAttr("right_button_two_text").isPresent()){
            rightBtnTwo.setText(attrSet.getAttr("right_button_two_text").get().getStringValue());
        }else{
            HiLog.error(LABEL,"attr right_button_two_text is not exist");
        }
        //处理右侧按钮的文本颜色
        if(attrSet.getAttr("right_button_two_text_color").isPresent()){
            rightBtnTwo.setTextColor(attrSet.getAttr("right_button_two_text_color").get().getColorValue());
        }else{
            HiLog.error(LABEL,"attr right_button_two_text_color is not exist");
        }
        //处理右侧按钮的文本大小
        if(attrSet.getAttr("right_button_two_text_size").isPresent()){
            rightBtnTwo.setTextSize(attrSet.getAttr("right_button_two_text_size").get().getIntegerValue(),Text.TextSizeType.FP);
        }else{
            HiLog.error(LABEL,"attr right_button_two_text_size is not exist");
        }
    }

    public CustomTitleBar(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

}
