package com.joanfuentes.hintcaseexample;

import com.joanfuentes.hintcaseexample.slice.CustomHintAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;

public class CustomHintAbility extends Ability {
    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(CustomHintAbilitySlice.class.getName());
    }
}
