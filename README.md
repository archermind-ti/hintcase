HintCase
==================
This library is the OHOS version based on https://github.com/Nescafemix/hintcase.   
Thanks to the original author at the very beginning.

HintCase is a library for OHOS that will help you create really awesome hints/tips for your apps. You can find a secondary library (HintCaseAssets) with multiple content holders and animators that you can use with HintCase library. Take a look to how you would integrate HintCase into your app.

With HintCase library you can apply shape animations to highlight a specific component and show a HintBlock and/or multiple ExtraBlocks all of them with optional animations to show/hide them. All is very easy, but if you don't have time to implement shapes, blocks and animations, with the HintCaseAssets library you can use some predefined items.

![HintCase demo](assets/welcome_demo.gif)


## How to use

### 1.- Configuring your Project Dependencies

Add the library dependency to your build.gradle file.

```groovy
dependencies {
    ...
    implementation 'com.gitee.archermind-ti:HintCase:1.0.0-beta'
}
```

#### 1.1- Optional Project Dependency

You can add the optional assets library dependency to your build.gradle file.

```groovy
dependencies {
    ...
    implementation 'com.gitee.archermind-ti:HintCaseAssets:1.0.0-beta'
}
```

### 2.- Showing a Hint

All you need to do is create a new HintCase object, configure it and call the **show()** method.

#### 2.1.- Passing a component to the constructor

You need to pass a component to the constructor of HintCase object. This component should be the parent where you want to show the Hint.
For example, if you are executing it from an Clicked event, you could use a **ComponentUtils.getRootComponent(component)** to get the top level ComponentContainer.

```java
protected void showHint() {
    ...
    parentComponent = ComponentUtils.getRootComponent(myButton);
    new HintCase(parentComponent)
                ...
                .show();
}
```

#### 2.2.- Configuring a Target (OPTIONAL)

You can configure a target component which will be highlighted. You can then configure the shape type, a margin between the shape and the target component (named offset) and if the target should be clickable or not.

If no offset is specified, the default offset value is 10dp.

```java
    .setTarget(findComponentById(ResourceTable.Id_text), new RectangularShape(), HintCase.TARGET_IS_NOT_CLICKABLE)
```

By default, if Target is not configured, a rectangular shape is used, the whole parent component is used as target, there is no offset and the target is not clickable.

In HintCaseAssets you can find an extra shape (CircularShape), which is ideal to use with small icons, and round buttons.

#### 2.3.- Configure animations to show/hide shapes (OPTIONAL)

You can show animations when you show/hide a target with a shape. To do it, call the method **setShapeAnimators(..)**. 
You can use a ShapeAnimator to show and another ShapeAnimator to hide. You can find some ShapeAnimators in HintCaseAssets library. Feel free to use them or create your own awesome animators.

##### FadeInShapeAnimator and FadeOutShapeAnimator

Fade in/out effects. You can set the time in the constructor. If no time is specified, 300ms are the default value.
You can configure a delay for the animation with setStartDelay(long startDelayTimeInMillis).

![Fade in/out animator demo](assets/fade_in_out_animator.gif)

##### RevealCircleShapeAnimator and UnrevealCircleShapeAnimator

Reveal/Unreveal effects to highlight a target with a circle shape. You can set the time in the constructor. If no time is specified, 300ms are the default value.
You can configure a delay for the animation with setStartDelay(long startDelayTimeInMillis).

![Reveal/unreveal circular animator demo](assets/reveal_unreveal_circle_animator.gif)

##### RevealRectangularShapeAnimator and UnrevealRectangularShapeAnimator

Reveal/Unreveal effects to highlight a target with a rectangular shape. You can set the time in the constructor. If no time is specified, 300ms are the default value.
You can configure a delay for the animation with setStartDelay(long startDelayTimeInMillis).

![Reveal/unreveal rectangular animator demo](assets/reveal_unreveal_rectangular_animator.gif)

Finally to configure shapeAnimators you need to call:

```java
    .setShapeAnimators(new RevealRectangularShapeAnimator(), ShapeAnimator.NO_ANIMATOR)
```

If you don't want an animation you can use ***ShapeAnimator.NO_ANIMATOR***

#### 2.4.- Configure a Background color (OPTIONAL)

You can specify a background color for the shape of the hint. You can use:

```java
    .setBackgroundColor(0x00000000)
```
 or
```java
     .setBackgroundColorByResourceId(ResourceTable.Color_colorPrimary)
```

#### 2.4.- Configure to close the hint on click on it (OPTIONAL)

By default, the hint is closed when the user click on it. If you don't want it, you can change this behavior with:

```java
    .setCloseOnTouchComponent(false)
```

#### 2.5.- Configure the Hint Block (OPTIONAL)

You can create a hint block which depends on the target position to show itself.
The hint block will be positioned on the biggest free space on the screen between the target component and the borders of the parent.
If no target was configured, the hint block will be positioned in the center of the parent component.
For example, if the parent component is the full screen, the hint block will be a component positioned in the center of the screen.

You can define your own HintContentHolder or use some of the existing in the HintCaseAssets library:

##### ByLayoutHintContentHolder

A HintContentHolder which inflates a specified layout. This is a fast way to create different hints with a specified layout for every hint.

```java
    ByLayoutHintContentHolder hintBlock = new ByLayoutHintContentHolder(ResourceTable.Layout_hint_welcome)
```

##### SimpleHintContentHolder

A HintContentHolder which can show a title, an image and a description. all 3 items are optional.

```java
    SimpleHintContentHolder hintBlock = new SimpleHintContentHolder.Builder(context)
               .setContentTitle(ResourceTable.String_title)
               .setContentText(ResourceTable.String_description)
               .setTitleStyle(ResourceTable.Pattern_title)
               .setContentStyle(ResourceTable.Pattern_content)
               .setImageResourceId(ResourceTable.Media_happy_welcome)
               .build();
```

The image can be passed as an image component instead of a picture so. In that case you need to use the **.setImage(imageComponent)** method.

Finally, you can configure ContentHolderAnimators to show or hide the hint block.

```java
    .setHintBlock(hintBlock, new FadeInContentHolderAnimator(), new FadeOutContentHolderAnimator())
```

If you don't want to create your own ContentHolderAnimators, you can use some existing in HintCaseAssets library as:

##### FadeInContentHolderAnimator and FadeOutContentHolderAnimator

Fade in/out effects. You can set the time in the constructor. If no time is specified, 300ms are the default value.
You can configure a delay for the animation with setStartDelay(long startDelayTimeInMillis).

![Fade in/out content holder animator demo](assets/fade_in_out_contentholder_animator.gif)

##### SlideInFromRightContentHolderAnimator and SlideOutFromRightContentHolderAnimator

Slide in/out movements from right position. You can set the time in the constructor. If no time is specified, 300ms are the default value.
You can configure a delay for the animation with setStartDelay(long startDelayTimeInMillis).

![Slide in/out content holder animator demo](assets/slide_in_out_contentholder_animator.gif)

#### 2.6.- Configure an Extra Block (OPTIONAL)

You can create multiple extra blocks which will be placed on a specified position of the parent component.

You can create your own ExtraContentHolders. You can find on HintCaseAssets library an example with SimpleButtonContentHolder:

##### SimpleButtonContentHolder

A HintContentHolder which show a button on the hint. It can be positioned based on rules of DependentLayout.

```java
SimpleButtonContentHolder okBlock = new SimpleButtonContentHolder.Builder(context)
                        .setWidth(ComponentContainer.LayoutConfig.MATCH_CONTENT)
                        .setHeight(ComponentContainer.LayoutConfig.MATCH_CONTENT)
                        .setRules(DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM, DependentLayout.LayoutConfig.ALIGN_PARENT_RIGHT)
                        .setButtonText(ResourceTable.String_ok)
                        .setCloseHintCaseOnClick(true)
                        .setButtonStyle(ResourceTable.Pattern_buttonNice)
                        .build();
```

You can configure the button to close the hint on clock with **.setCloseHintCaseOnClick(true)** and define the button theme with **.setButtonStyle(ResourceTable.Pattern_buttonNice)**.

Finally, you can configure ContentHolderAnimators to show or hide the extra block.

```java
    .setExtraBlock(okBlock, new SlideInFromRightContentHolderAnimator(), new SlideOutFromRightContentHolderAnimator())
```

If you don't want to create your own ContentHolderAnimators, you can use some existing in HintCaseAssets library.

#### 2.7.- Configure a listener when the hint is closed (OPTIONAL)

You can set a listener to execute when the hint is closed:

```java
    .setOnClosedListener(new HintCase.OnClosedListener() {
        @Override
        public void onClosed() {
            new ToastDialog(getContext()).setText("Hint closed").show()
        }
     })
```

### 3.- Concatenate several hints (OPTIONAL)

You can concatenate several hints simply using the OnClosedListener to launch the next HintCase. Even you can configure hints to avoid some animations so that the user does not perceive that are different hints.

For example:
```java
    SimpleHintContentHolder hintBlock = new SimpleHintContentHolder.Builder(component.getContext())
            .setContentTitle("Attention!")
            .setContentText("You can find here your notifications")
            .setTitleStyle(ResourceTable.Pattern_title)
            .setContentStyle(ResourceTable.Pattern_content)
            .build();
    new HintCase(ComponentUtils.getRootComponent(component))
            .setTarget(findComponentById(ResourceTable.Id_text), new CircularShape(), HintCase.TARGET_IS_NOT_CLICKABLE)
            .setBackgroundColor(getColor(ResourceTable.Color_colorPrimary))
            .setShapeAnimators(new RevealCircleShapeAnimator(), ShapeAnimator.NO_ANIMATOR)
            .setHintBlock(hintBlock, new FadeInContentHolderAnimator(), new SlideOutFromRightContentHolderAnimator())
            .setOnClosedListener(new HintCase.OnClosedListener() {
                @Override
                public void onClosed() {
                    SimpleHintContentHolder secondHintBlock = new SimpleHintContentHolder.Builder(component.getContext())
                            .setContentTitle("Notifications center is your best friend")
                            .setContentText("Every time you purchase a game, a notification will be generated")
                            .setTitleStyle(ResourceTable.Pattern_title)
                            .setContentStyle(ResourceTable.Pattern_content)
                            .build();
                    new HintCase(ComponentUtils.getRootComponent(component))
                            .setTarget(findComponentById(ResourceTable.Id_text), new CircularShape())
                            .setBackgroundColor(getColor(ResourceTable.Color_colorPrimary))
                            .setShapeAnimators(ShapeAnimator.NO_ANIMATOR, new UnrevealCircleShapeAnimator())
                            .setHintBlock(secondHintBlock, new SlideInFromRightContentHolderAnimator())
                            .show();
                }
            })
            .show();
```

In this example, the fist hint is configured to show a reveal animation for the shape but with no animation to hide the hint. The next hint doesn't show an animation to show so, the effect is that the shape was fixed and there was no change with it.
The hint block was showed with a fade in animation but to hide it was shown an slideOut animation, and the hint block in the next screen was showed with a slideIn animation.
The result was this:

![Consecutive hints demo](assets/consecutive_hints.gif)

### 4.- Building your own ContentHolder (OPTIONAL)

Building your own content holder is really easy. If you want that you contentHolder was showed on the biggest free space in the screen you should extend from HintContentHolder. Otherwise, if you want that your ContentHolder can fill all the parent component extends from ExtraContentHolder.
Both of the base classes (HintContentHolder and ExtraContentHolder) extends from ContentHolder and define its specific getParentLayoutConfig to use in your custom ContentHolder.

In your custom ContentHolder you will need to define your own getComponent method to return the component that you generated for your hint.
You receive the context, the parent componentContainer and the full hintcase so you can even know where the target is to mount your own contentHolder.
```java
    @Override
    public Component getComponent(Context context, final HintCase hintCase, ComponentContainer parent) {
        ...
    }
```

as an optional you can define the onLayout method that you listen when the layout is parsed so you can configure your contentHolder based on the calculated position for the HintContentHolder
```ave
    @Override
    public void onArrange() {
        ...
    }
```
In the app example you can find a CustomHintContentHolder that override and use both methods.

### 5.- Building your own ContentHolderAnimator (OPTIONAL)

Building your own content holder animator is really easy. You should extend from ContentHolderAnimator.
 
 In Your custom ContentHolderAnimator you will need to define your own getAnimator method to return the Animator of the contentHolder.
 You receive the component for which is the animator applied and a onFinishListener to call when the animator is finished.

```java
    @Override
    public Animator getAnimator(Component component, OnFinishListener onFinishListener) {
        ...
    }
```

### 6.- Building your own Shape (OPTIONAL)

A rectangular and a Circular shape are provided with the main library and the assets library, but if you need to do your own shape, you can do it!.

In your Shape you will need to define some methods:
```java
    @Override
    public void setMinimumValue() {
        ...
    }
    
    @Override
    public void setShapeInfo(Component targetComponent, ComponentContainer parent, int offset, Context context) {
        ...
    }
    
     public boolean isTouchEventInsideTheHint(TouchEvent event) {
        ...
    }
    
    public void draw(Canvas canvas) {
        ...
    }

```
- setMinimumValue : you should set the minimum size of the shape in this method.
- setShapeInfo : you should set all the necessary info of the shape for use on ShapeAnimators.
- isTouchEventInsideTheHint: this method should return if the user performed a touch event inside the Highlighted item.
- draw: this method should draw the shape in the canvas, current I use canvas.clipPath(path, Canvas.ClipOp.DIFFERENCE) before draw HintCase's background to leave the shape empty;

You can check the code of CircularShape & RectangularShape to se some examples.

### 7.- Building your own ShapeAnimator (OPTIONAL)

Building your own shape animator is really easy. You should extend from ShapeAnimator.
 
 In Your custom ShapeAnimator wou will need to define your own getAnimator method to return the Animator of the contentHolder.
 You receive the component on which is the animator applied, the shape to animate and a onFinishListener to call when the animator is finished.

```java
    @Override
    public Animator getAnimator(Component component, Shape shape, final OnFinishListener onFinishListener) {
        ...
    }
```

## License

Copyright Joan Fuentes 2016

This file is part of some open source application.

Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
