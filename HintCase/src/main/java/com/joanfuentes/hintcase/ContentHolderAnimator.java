package com.joanfuentes.hintcase;

import ohos.agp.animation.Animator;
import ohos.agp.components.Component;

public abstract class ContentHolderAnimator {
    public static final int DEFAULT_ANIMATION_DURATION_IN_MILLISECONDS = 300;
    public static final ContentHolderAnimator NO_ANIMATOR = null;
    public static final OnFinishListener NO_CALLBACK = null;
    protected final int durationInMilliseconds;
    protected long startDelayInMilliseconds;

    public ContentHolderAnimator() {
        durationInMilliseconds = DEFAULT_ANIMATION_DURATION_IN_MILLISECONDS;
    }

    public ContentHolderAnimator(int durationInMilliseconds) {
        this.durationInMilliseconds = durationInMilliseconds;
    }

    public Animator getAnimator(Component component) {
        return getAnimator(component, NO_CALLBACK);
    }

    public abstract Animator getAnimator(Component component, OnFinishListener onFinishListener);

    public ContentHolderAnimator setStartDelay(long startDelayInMilliseconds) {
        this.startDelayInMilliseconds = startDelayInMilliseconds;
        return this;
    }

    public interface OnFinishListener {
        void onFinish();
    }
}
