package com.joanfuentes.hintcase;

import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorGroup;
import ohos.agp.components.*;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.ArrayList;
import java.util.List;

class HintCaseComponent extends DependentLayout implements ComponentContainer.ArrangeListener, Component.TouchEventListener, Component.DrawTask {
    private static final int DEFAULT_BACKGROUND_COLOR = 0xCC000000;
    private static final int DEFAULT_HINT_BLOCK_POSITION = HintCase.HINT_BLOCK_POSITION_BOTTOM;
    private static final Shape DEFAULT_SHAPE = new RectangularShape();
    private static final ContentHolder NO_BLOCK_INFO = null;
    private static final Component NO_BLOCK_INFO_COMPONENT = null;
    private static final Component NO_TARGET_COMPONENT = null;

    private Component targetComponent;
    private Shape shape = DEFAULT_SHAPE;
    private ContentHolder hintBlock;
    private Paint basePaint;
    private Component hintBlockComponent;
    private ComponentContainer parent;
    private int parentIndex;
    private int backgroundColor;
    private int offsetInPx;
    private int hintBlockPosition;
    private HintCase.OnClosedListener onClosedListener;
    private ShapeAnimator showShapeAnimator;
    private ShapeAnimator hideShapeAnimator;
    private ContentHolderAnimator showContentHolderAnimator;
    private ContentHolderAnimator hideContentHolderAnimator;
    private List<ContentHolder> extraBlocks;
    private List<Component> extraBlockComponents;
    private List<ContentHolderAnimator> showExtraContentHolderAnimators;
    private List<ContentHolderAnimator> hideExtraContentHolderAnimators;
    private boolean closeOnTouch;
    private HintCase hintCase;
    private boolean isTargetClickable;
    private boolean wasPressedOnShape;

    public Component getHintBlockComponent() {
        return hintBlockComponent;
    }

    public HintCaseComponent(Context context, HintCase hintCase) {
        super(context);
        addDrawTask(this, BETWEEN_BACKGROUND_AND_CONTENT);
        setArrangeListener(this);
        setTouchEventListener(this);
        setClickedListener(component -> {
        });
        init(hintCase);
    }

    private void init(HintCase hintCase) {
        setLayoutConfig(new LayoutConfig(ComponentContainer.LayoutConfig.MATCH_PARENT,
                ComponentContainer.LayoutConfig.MATCH_PARENT));
        this.hintCase = hintCase;
        closeOnTouch = true;
        showExtraContentHolderAnimators = new ArrayList<>();
        hideExtraContentHolderAnimators = new ArrayList<>();
        hintBlock = NO_BLOCK_INFO;
        hintBlockComponent = NO_BLOCK_INFO_COMPONENT;
        extraBlocks = new ArrayList<>();
        extraBlockComponents = new ArrayList<>();
        backgroundColor = DEFAULT_BACKGROUND_COLOR;
        offsetInPx = HintCase.NO_OFFSET_IN_PX;
        hintBlockPosition = DEFAULT_HINT_BLOCK_POSITION;
        basePaint = new Paint();
        basePaint.setAntiAlias(true);
    }

    private void performShow() {
        parent.addComponent(this, parentIndex);
        if (showShapeAnimator != ShapeAnimator.NO_ANIMATOR) {
            Animator animator = showShapeAnimator.getAnimator(this, shape, this::performShowBlocks);
            animator.start();
        } else {
            shape.setMinimumValue();
            parent.setLayoutRefreshedListener(component -> {
                performShowBlocks();
                parent.setLayoutRefreshedListener(null);
            });
        }
    }

    private void performShowBlocks() {
        List<Animator> animators = new ArrayList<>();
        if (showContentHolderAnimator != ContentHolderAnimator.NO_ANIMATOR) {
            animators.add(showContentHolderAnimator.getAnimator(hintBlockComponent));
        }
        if (!showExtraContentHolderAnimators.isEmpty()) {
            for (int i = 0; i < extraBlocks.size(); i++) {
                ContentHolderAnimator animator = showExtraContentHolderAnimators.get(i);
                if (animator != ContentHolderAnimator.NO_ANIMATOR) {
                    animators.add(animator.getAnimator(extraBlockComponents.get(i)));
                }
            }
        }
        AnimatorGroup animatorSet = new AnimatorGroup();
        if (animators.isEmpty()) {
            if (existHintBlock()) {
                getHintBlockComponent().setVisibility(Component.VISIBLE);
            }
            for (Component component : extraBlockComponents) {
                component.setVisibility(Component.VISIBLE);
            }
        } else {
            animatorSet.runParallel(animators.toArray(new Animator[0]));
            animatorSet.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {

                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    if (existHintBlock()
                            && showContentHolderAnimator == ContentHolderAnimator.NO_ANIMATOR) {
                        getHintBlockComponent().setVisibility(Component.VISIBLE);
                    }
                    for (int i = 0; i < showExtraContentHolderAnimators.size(); i++) {
                        ContentHolderAnimator holderAnimator = showExtraContentHolderAnimators.get(i);
                        if (holderAnimator == ContentHolderAnimator.NO_ANIMATOR) {
                            extraBlockComponents.get(i).setVisibility(Component.VISIBLE);
                        }
                    }
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
            animatorSet.start();
        }
    }

    void performHide() {
        List<Animator> animators = new ArrayList<>();
        if (hideContentHolderAnimator != ContentHolderAnimator.NO_ANIMATOR) {
            animators.add(hideContentHolderAnimator.getAnimator(hintBlockComponent));
        } else {
            if (existHintBlock()) {
                getHintBlockComponent().setVisibility(Component.INVISIBLE);
            }
        }
        if (!hideExtraContentHolderAnimators.isEmpty()) {
            for (int i = 0; i < extraBlocks.size(); i++) {
                ContentHolderAnimator animator = hideExtraContentHolderAnimators.get(i);
                if (animator != ContentHolderAnimator.NO_ANIMATOR) {
                    animators.add(animator.getAnimator(extraBlockComponents.get(i)));
                }
            }
        }
        AnimatorGroup animatorSet = new AnimatorGroup();
        if (animators.isEmpty()) {
            for (Component component : extraBlockComponents) {
                component.setVisibility(Component.INVISIBLE);
            }
            performHideShape();
        } else {
            animatorSet.runParallel(animators.toArray(new Animator[0]));
            animatorSet.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {

                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    performHideShape();
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
            animatorSet.start();
        }
    }

    private void performHideShape() {
        List<Animator> animators = new ArrayList<>();
        if (hideShapeAnimator != ShapeAnimator.NO_ANIMATOR) {
            animators.add(hideShapeAnimator.getAnimator(this, shape));
        }
        AnimatorGroup animatorSet = new AnimatorGroup();
        if (animators.isEmpty()) {
            close();
        } else {
            animatorSet.runSerially(animators.toArray(new Animator[0]));
            animatorSet.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {

                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {

                }

                @Override
                public void onEnd(Animator animator) {
                    close();
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
            animatorSet.start();
        }
    }

    private void close() {
        removeComponent();
        clearData();
        if (onClosedListener != null) {
            onClosedListener.onClosed();
        }
    }

    private void clearData() {
        parent = null;
        hintCase = null;
    }

    private void removeComponent() {
        if (parent != null) {
            parent.removeComponent(this);
        }
    }

    private void setComponents() {
        if (existHintBlock()) {
            StackLayout stackLayout = getHintBlockStackLayout();
            if (hintBlockComponent == NO_BLOCK_INFO_COMPONENT) {
                hintBlockComponent = hintBlock.getComponent(getContext(), hintCase, stackLayout);
                hintBlockComponent.setVisibility(Component.INVISIBLE);
            }
            stackLayout.addComponent(hintBlockComponent);
            addComponent(stackLayout);
        }
        if (existExtraBlock()) {
            DependentLayout dependentLayout = getExtraContentHolderDependentLayout();
            for (int i = 0; i < extraBlocks.size(); i++) {
                Component component = extraBlocks.get(i).getComponent(getContext(), hintCase, dependentLayout);
                if (showExtraContentHolderAnimators.get(i) != ContentHolderAnimator.NO_ANIMATOR) {
                    component.setVisibility(Component.INVISIBLE);
                }
                extraBlockComponents.add(component);
                dependentLayout.addComponent(component);
            }
            addComponent(dependentLayout);
        }
    }

    private boolean existHintBlock() {
        return hintBlock != NO_BLOCK_INFO;
    }

    private boolean existExtraBlock() {
        return !extraBlocks.isEmpty();
    }

    private StackLayout getHintBlockStackLayout() {
        int blockWidth = 0;
        int blockHeight = 0;
        int blockAlign = 0;
        switch (hintBlockPosition) {
            case HintCase.HINT_BLOCK_POSITION_TOP:
                blockWidth = parent.getWidth();
                blockHeight = shape.getTop() - parent.getTop();
                blockAlign = DependentLayout.LayoutConfig.ALIGN_PARENT_TOP;
                break;
            case HintCase.HINT_BLOCK_POSITION_BOTTOM:
                blockWidth = parent.getWidth();
                blockHeight = parent.getBottom() - shape.getBottom();
                blockAlign = DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM;
                break;
            case HintCase.HINT_BLOCK_POSITION_LEFT:
                blockWidth = shape.getLeft() - parent.getLeft();
                blockHeight = parent.getHeight();
                blockAlign = DependentLayout.LayoutConfig.ALIGN_PARENT_LEFT;
                break;
            case HintCase.HINT_BLOCK_POSITION_RIGHT:
                blockWidth = parent.getRight() - shape.getRight();
                blockHeight = parent.getHeight();
                blockAlign = DependentLayout.LayoutConfig.ALIGN_PARENT_RIGHT;
                break;
            case HintCase.HINT_BLOCK_POSITION_CENTER:
                blockWidth = parent.getWidth();
                blockHeight = parent.getHeight();
                blockAlign = DependentLayout.LayoutConfig.ALIGN_PARENT_BOTTOM;
                break;
            default:
                break;
        }
        LayoutConfig dependentLayoutConfig = new LayoutConfig(blockWidth, blockHeight);
        dependentLayoutConfig.addRule(blockAlign);
        StackLayout stackLayout = new StackLayout(getContext());
        stackLayout.setLayoutConfig(dependentLayoutConfig);
        return stackLayout;
    }

    private DependentLayout getExtraContentHolderDependentLayout() {
        LayoutConfig dependentLayoutConfig = new LayoutConfig(parent.getWidth(), parent.getHeight());
        DependentLayout dependentLayout = new DependentLayout(getContext());
        dependentLayout.setLayoutConfig(dependentLayoutConfig);
        return dependentLayout;
    }

    private void calculateHintBlockPosition(ComponentContainer parent, Shape shape) {
        if (targetComponent == NO_TARGET_COMPONENT) {
            hintBlockPosition = HintCase.HINT_BLOCK_POSITION_CENTER;
        } else {
            int[] areas = new int[4];
            areas[HintCase.HINT_BLOCK_POSITION_LEFT] = shape.getLeft() - parent.getLeft();
            areas[HintCase.HINT_BLOCK_POSITION_TOP] = shape.getTop() - parent.getTop();
            areas[HintCase.HINT_BLOCK_POSITION_RIGHT] = parent.getRight() - shape.getRight();
            areas[HintCase.HINT_BLOCK_POSITION_BOTTOM] = parent.getBottom() - shape.getBottom();
            hintBlockPosition = HintCase.HINT_BLOCK_POSITION_LEFT;
            for (int i = 1; i < areas.length; i++) {
                if (areas[i] >= areas[hintBlockPosition]) {
                    hintBlockPosition = i;
                }
            }
        }
    }

    public void setOnClosedListener(HintCase.OnClosedListener onClickListener) {
        this.onClosedListener = onClickListener;
    }

    public void setTargetInfo(Component component, Shape shape, int offsetInPx, boolean isTargetClickable) {
        this.targetComponent = component;
        this.shape = shape;
        this.offsetInPx = offsetInPx;
        this.isTargetClickable = isTargetClickable;
    }

    public void setOverDecorComponent(Component decorComponent) {
        parent = ((ComponentContainer) decorComponent);
        parentIndex = -1;
    }

    public void setParentComponent(Component parentComponent) {
        parent = (ComponentContainer) parentComponent;
        parentIndex = parent.getChildCount();
    }

    public void setCloseOnTouch(boolean closeOnTouch) {
        this.closeOnTouch = closeOnTouch;
    }

    public void show() {
        initializeComponent();
        performShow();
    }

    public void initializeComponent() {
        shape.setShapeInfo(targetComponent, parent, offsetInPx, getContext());
        calculateHintBlockPosition(parent, shape);
        setComponents();
    }

    public Shape getShape() {
        return shape;
    }

    public void setShape(ShapeAnimator showShapeAnimator,
                         ShapeAnimator hideShapeAnimator) {
        this.showShapeAnimator = showShapeAnimator;
        this.hideShapeAnimator = hideShapeAnimator;
    }

    public void setHintBlock(ContentHolder contentHolder, ContentHolderAnimator showContentHolderAnimator,
                             ContentHolderAnimator hideContentHolderAnimator) {
        this.hintBlock = contentHolder;
        this.showContentHolderAnimator = showContentHolderAnimator;
        this.hideContentHolderAnimator = hideContentHolderAnimator;
    }

    public int getHintBlockPosition() {
        return hintBlockPosition;
    }

    public void setExtraBlock(ContentHolder contentHolder, ContentHolderAnimator showContentHolderAnimator,
                              ContentHolderAnimator hideContentHolderAnimator) {
        if (contentHolder != null) {
            this.extraBlocks.add(contentHolder);
            this.showExtraContentHolderAnimators.add(showContentHolderAnimator);
            this.hideExtraContentHolderAnimators.add(hideContentHolderAnimator);
        }
    }

    @Override
    public boolean onArrange(int left, int top, int right, int bottom) {
        if (hintBlock != NO_BLOCK_INFO) {
            hintBlock.onArrange();
        }
        for (ContentHolder extraBlock : extraBlocks) {
            extraBlock.onArrange();
        }
        return false;
    }


    @Override
    public boolean onTouchEvent(Component component, TouchEvent event) {
        switch (event.getAction()) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                wasPressedOnShape = shape.isTouchEventInsideTheHint(event);
                break;
            case TouchEvent.POINT_MOVE:
                if (!shape.isTouchEventInsideTheHint(event))
                    wasPressedOnShape = false;
                break;
            case TouchEvent.PRIMARY_POINT_UP:
                if (closeOnTouch) {
                    performHide();
                }
                if (targetComponent != null
                        && isTargetClickable
                        && wasPressedOnShape && shape.isTouchEventInsideTheHint(event)) {
                    targetComponent.simulateClick();
                }
                break;
            default:
                break;
        }
        return true;
    }

    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (parent.getEstimatedWidth() > 0 && parent.getEstimatedHeight() > 0) {
            if (shape != null
                    && (showShapeAnimator != ShapeAnimator.NO_ANIMATOR
                    || hideShapeAnimator != ShapeAnimator.NO_ANIMATOR)) {
                shape.draw(canvas);
            }
            canvas.drawRect(0, 0, parent.getEstimatedWidth(), parent.getEstimatedHeight(), basePaint, new Color(backgroundColor));
        }
    }

    public void setBackgroundColor(int color) {
        backgroundColor = color;
    }
}