package com.joanfuentes.hintcase;


import ohos.agp.animation.Animator;
import ohos.agp.components.Component;

public abstract class ShapeAnimator {
    public static final int DEFAULT_ANIMATION_DURATION_IN_MILLISECONDS = 300;
    public static final ShapeAnimator NO_ANIMATOR = null;
    public static final OnFinishListener NO_CALLBACK = null;
    protected final int durationInMilliseconds;
    protected long startDelayInMilliseconds;

    public ShapeAnimator() {
        durationInMilliseconds = DEFAULT_ANIMATION_DURATION_IN_MILLISECONDS;
    }

    public ShapeAnimator(int durationInMilliseconds) {
        this.durationInMilliseconds = durationInMilliseconds;
    }

    public abstract Animator getAnimator(Component component, Shape shape, OnFinishListener onFinishListener);

    public Animator getAnimator(Component component, Shape shape) {
        return getAnimator(component, shape, NO_CALLBACK);
    }

    public ShapeAnimator setStartDelay(long startDelayInMilliseconds) {
        this.startDelayInMilliseconds = startDelayInMilliseconds;
        return this;
    }

    public interface OnFinishListener {
        void onFinish();
    }
}