package com.joanfuentes.hintcase;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.Path;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.multimodalinput.event.MmiPoint;
import ohos.multimodalinput.event.TouchEvent;

public class RectangularShape extends Shape {
    private static final float DEFAULT_MIN_HEIGHT = 50;
    private static final float DEFAULT_MAX_HEIGHT = 10000;
    private static final float DEFAULT_MIN_WIDTH = 50;
    private static final float DEFAULT_MAX_WIDTH = 10000;

    private float maxHeight = DEFAULT_MAX_HEIGHT;
    private float minHeight = DEFAULT_MIN_HEIGHT;
    private float minWidth = DEFAULT_MIN_WIDTH;
    private float maxWidth = DEFAULT_MAX_WIDTH;
    private float currentHeight = DEFAULT_MAX_HEIGHT;
    private float currentWidth = DEFAULT_MAX_WIDTH;

    public RectangularShape() {
        super();
    }

    @Override
    public void setMinimumValue() {
        currentWidth = minWidth;
        currentHeight = minHeight;
    }

    @Override
    public void setShapeInfo(Component targetComponent, ComponentContainer parent, int offset, Context context) {
        if (targetComponent != null && parent != null) {
            minHeight = targetComponent.getEstimatedHeight() + (offset * 2);
            minWidth = targetComponent.getEstimatedWidth() + (offset * 2);
            maxHeight = parent.getHeight() * 2;
            maxWidth = parent.getWidth() * 2;
            int[] targetComponentLocationInWindow = targetComponent.getLocationOnScreen();
            int[] parentLocationInWindow = parent.getLocationOnScreen();
            targetComponentLocationInWindow[0] = targetComponentLocationInWindow[0] - parentLocationInWindow[0];
            targetComponentLocationInWindow[1] = targetComponentLocationInWindow[1] - parentLocationInWindow[1];
            setCenterX(targetComponentLocationInWindow[0] + targetComponent.getWidth() / 2f);
            setCenterY(targetComponentLocationInWindow[1] + targetComponent.getHeight() / 2f);
            setLeft(targetComponentLocationInWindow[0] - offset);
            setRight(targetComponentLocationInWindow[0] + (int) minWidth - offset);
            setTop(targetComponentLocationInWindow[1] - offset);
            setBottom(targetComponentLocationInWindow[1] + (int) minHeight - offset);
            setWidth(minWidth);
            setHeight(minHeight);
        } else if (parent != null) {
            minHeight = 0;
            minWidth = 0;
            maxHeight = parent.getHeight();
            maxWidth = parent.getWidth();
            setCenterX(parent.getEstimatedWidth() / 2f);
            setCenterY(parent.getEstimatedHeight() / 2f);
            setLeft(0);
            setTop(0);
            setRight(0);
            setBottom(0);
        }
        currentHeight = maxHeight;
        currentWidth = maxWidth;
    }

    @Override
    public boolean isTouchEventInsideTheHint(TouchEvent event) {
        MmiPoint point = event.getPointerPosition(event.getIndex());
        return point.getX() <= getRight()
                && point.getX() >= getLeft()
                && point.getY() <= getBottom()
                && point.getY() >= getTop();
    }

    @Override
    public void draw(Canvas canvas) {
        Path p = getShapePath();
        p.reset();
        p.addRoundRect(new RectFloat(getCenterX() - (currentWidth / 2),
                getCenterY() - (currentHeight / 2),
                getCenterX() + (currentWidth / 2),
                getCenterY() + (currentHeight / 2)), 10f, 10f, Path.Direction.CLOCK_WISE);
        canvas.clipPath(p, Canvas.ClipOp.DIFFERENCE);
    }

    public float getMinHeight() {
        return minHeight;
    }

    public float getMaxHeight() {
        return maxHeight;
    }

    public float getMinWidth() {
        return minWidth;
    }

    public float getMaxWidth() {
        return maxWidth;
    }

    public float getCurrentHeight() {
        return currentHeight;
    }

    public void setCurrentHeight(float currentHeight) {
        this.currentHeight = currentHeight;
    }

    public void setCurrentWidth(float currentWidth) {
        this.currentWidth = currentWidth;
    }
}
