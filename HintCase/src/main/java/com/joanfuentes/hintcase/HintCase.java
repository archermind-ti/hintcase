package com.joanfuentes.hintcase;


import com.joanfuentes.hintcase.utils.ComponentUtils;
import ohos.agp.components.Component;
import ohos.app.Context;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;

import java.io.IOException;

public class HintCase {
    public static final int DEFAULT_SHAPE_OFFSET_IN_DP = 10;
    public static final int NO_OFFSET_IN_PX = 0;
    public static final int BACKGROUND_COLOR_TRANSPARENT = 0x00000000;
    public static final int HINT_BLOCK_POSITION_LEFT = 0;
    public static final int HINT_BLOCK_POSITION_TOP = 1;
    public static final int HINT_BLOCK_POSITION_RIGHT = 2;
    public static final int HINT_BLOCK_POSITION_BOTTOM = 3;
    public static final int HINT_BLOCK_POSITION_CENTER = 4;
    public static final boolean TARGET_IS_CLICKABLE = true;
    public static final boolean TARGET_IS_NOT_CLICKABLE = false;

    private HintCaseComponent hintCaseComponent;
    private final Context context;

    public Shape getShape() {
        return this.hintCaseComponent.getShape();
    }

    public void hide() {
        this.hintCaseComponent.performHide();
        this.hintCaseComponent = null;
    }

    public HintCase(Component component) {
        this.context = component.getContext();
        this.hintCaseComponent = new HintCaseComponent(context, this);
        this.hintCaseComponent.setTargetInfo(null, new RectangularShape(), NO_OFFSET_IN_PX,
                TARGET_IS_NOT_CLICKABLE);
        this.hintCaseComponent.setParentComponent(component);
    }

    public Component getComponent() {
        return this.hintCaseComponent;
    }

    public HintCase setTarget(Component target) {
        return setCompleteTarget(target, new RectangularShape(), DEFAULT_SHAPE_OFFSET_IN_DP, TARGET_IS_CLICKABLE);
    }

    public HintCase setTarget(Component target, int offsetResId) {
        return setCompleteTarget(target, new RectangularShape(), getVpByResId(offsetResId), TARGET_IS_CLICKABLE);
    }

    public HintCase setTarget(Component target, boolean isTargetClickable) {
        return setCompleteTarget(target, new RectangularShape(), HintCase.DEFAULT_SHAPE_OFFSET_IN_DP, isTargetClickable);
    }

    public HintCase setTarget(Component target, boolean isTargetClickable, int offsetResId) {
        return setCompleteTarget(target, new RectangularShape(), getVpByResId(offsetResId), isTargetClickable);
    }

    public HintCase setTarget(Component target, Shape shape) {
        return setCompleteTarget(target, shape, DEFAULT_SHAPE_OFFSET_IN_DP, TARGET_IS_CLICKABLE);
    }

    public HintCase setTarget(Component target, Shape shape, int offsetResId) {
        return setCompleteTarget(target, shape, getVpByResId(offsetResId), TARGET_IS_CLICKABLE);
    }

    public HintCase setTarget(Component target, Shape shape, boolean isTargetClickable) {
        return setCompleteTarget(target, shape, DEFAULT_SHAPE_OFFSET_IN_DP, isTargetClickable);
    }

    public HintCase setTarget(Component target, Shape shape, boolean isTargetClickable, int offsetResId) {
        return setCompleteTarget(target, shape, getVpByResId(offsetResId), isTargetClickable);
    }

    private int getVpByResId(int resId) {
        try {
            return context.getResourceManager().getElement(resId).getInteger();
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
            return 0;
        }
    }

    private HintCase setCompleteTarget(Component target, Shape shape, int offsetInVp, boolean isTargetClickable) {
        this.hintCaseComponent.setTargetInfo(target, shape, ComponentUtils.vpToPixels(context, offsetInVp), isTargetClickable);
        return this;
    }

    public HintCase setBackgroundColor(int color) {
        this.hintCaseComponent.setBackgroundColor(color);
        return this;
    }

    public HintCase setBackgroundColorByResourceId(int resourceId) {
        this.hintCaseComponent.setBackgroundColor(context.getColor(resourceId));
        return this;
    }

    public HintCase setShapeAnimators(ShapeAnimator showShapeAnimator) {
        return setShapeAnimators(showShapeAnimator, ShapeAnimator.NO_ANIMATOR);
    }

    public HintCase setShapeAnimators(ShapeAnimator showShapeAnimator,
                                      ShapeAnimator hideShapeAnimator) {
        this.hintCaseComponent.setShape(showShapeAnimator, hideShapeAnimator);
        return this;
    }

    public HintCase setHintBlock(ContentHolder contentHolder) {
        this.hintCaseComponent.setHintBlock(contentHolder, ContentHolderAnimator.NO_ANIMATOR, ContentHolderAnimator.NO_ANIMATOR);
        return this;
    }

    public HintCase setHintBlock(ContentHolder contentHolder, ContentHolderAnimator showContentHolderAnimator) {
        this.hintCaseComponent.setHintBlock(contentHolder, showContentHolderAnimator, ContentHolderAnimator.NO_ANIMATOR);
        return this;
    }

    public HintCase setHintBlock(ContentHolder contentHolder, ContentHolderAnimator showContentHolderAnimator,
                                 ContentHolderAnimator hideContentHolderAnimator) {
        this.hintCaseComponent.setHintBlock(contentHolder, showContentHolderAnimator, hideContentHolderAnimator);
        return this;
    }

    public HintCase setExtraBlock(ContentHolder contentHolder) {
        this.hintCaseComponent.setExtraBlock(contentHolder, ContentHolderAnimator.NO_ANIMATOR, ContentHolderAnimator.NO_ANIMATOR);
        return this;
    }

    public HintCase setExtraBlock(ContentHolder contentHolder, ContentHolderAnimator showContentHolderAnimator) {
        this.hintCaseComponent.setExtraBlock(contentHolder, showContentHolderAnimator, ContentHolderAnimator.NO_ANIMATOR);
        return this;
    }

    public HintCase setExtraBlock(ContentHolder contentHolder, ContentHolderAnimator showContentHolderAnimator,
                                  ContentHolderAnimator hideContentHolderAnimator) {
        this.hintCaseComponent.setExtraBlock(contentHolder, showContentHolderAnimator, hideContentHolderAnimator);
        return this;
    }

    public HintCase setCloseOnTouchComponent(boolean closeOnTouch) {
        this.hintCaseComponent.setCloseOnTouch(closeOnTouch);
        return this;
    }

    public HintCase setOverDecorComponent(boolean setOver, Component decorComponent) {
        if (setOver) {
            this.hintCaseComponent.setOverDecorComponent(decorComponent);
        }
        return this;
    }

    public HintCase setOnClosedListener(OnClosedListener onClosedListener) {
        this.hintCaseComponent.setOnClosedListener(onClosedListener);
        return this;
    }

    public int getBlockInfoPosition() {
        return this.hintCaseComponent.getHintBlockPosition();
    }

    public void show() {
        //TODO: Add controls to have minimum info.
        this.hintCaseComponent.show();
    }

    public interface OnClosedListener {
        void onClosed();
    }
}
