/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.joanfuentes.hintcase.utils;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.element.ElementScatter;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.global.configuration.DeviceCapability;
import ohos.global.resource.NotExistException;
import ohos.global.resource.WrongTypeException;
import ohos.global.resource.solidxml.TypedAttribute;

import java.io.IOException;
import java.util.HashMap;

public final class ComponentUtils {

    private ComponentUtils() {
    }

    public static void applyStyle(Context context, Text text, int styleId) {
        try {
            HashMap<String, TypedAttribute> style = context.getResourceManager().getElement(styleId).getPattern().getPatternHash();
            if (style.containsKey("text_color")) {
                text.setTextColor(new Color(style.get("text_color").getColorValue()));
            }
            if (style.containsKey("text_size")) {
                text.setTextSize(style.get("text_size").getIntegerValue(), Text.TextSizeType.VP);
            }
            if (style.containsKey("background_color")) {
                ShapeElement shapeElement = new ShapeElement();
                shapeElement.setRgbColor(RgbColor.fromArgbInt(style.get("background_color").getColorValue()));
                text.setBackground(shapeElement);
            }
            if (style.containsKey("background_element")) {
                text.setBackground(new ElementScatter(context).parse(style.get("background_element").getResId()));
            }
            if (style.containsKey("horizontal_padding")) {
                int padding = vpToPixels(context, style.get("horizontal_padding").getIntegerValue());
                text.setHorizontalPadding(padding, padding);
            }
            if (style.containsKey("vertical_padding")) {
                int padding = vpToPixels(context, style.get("vertical_padding").getIntegerValue());
                text.setVerticalPadding(padding, padding);
            }
        } catch (IOException | NotExistException | WrongTypeException e) {
            e.printStackTrace();
        }
    }

    public static Component getRootComponent(Component component) {
        while (component.getComponentParent() != null) {
            component = (Component) component.getComponentParent();
        }
        return component;
    }

    public static int vpToPixels(Context context, float valueInVP) {
        int density = context.getResourceManager().getDeviceCapability().screenDensity / DeviceCapability.SCREEN_MDPI;
        return (int) (valueInVP * density);
    }
}
