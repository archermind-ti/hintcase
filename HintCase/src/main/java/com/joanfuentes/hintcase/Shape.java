package com.joanfuentes.hintcase;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.render.Canvas;
import ohos.agp.render.Path;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

public abstract class Shape {
    private final Path path;
    private int left;
    private int top;
    private int right;
    private int bottom;
    private float centerX;
    private float centerY;
    private float width;
    private float height;

    public Shape() {
        path = new Path();
    }

    public int getLeft() {
        return left;
    }

    public void setLeft(int left) {
        this.left = left;
    }

    public int getTop() {
        return top;
    }

    public void setTop(int top) {
        this.top = top;
    }

    public int getRight() {
        return right;
    }

    public void setRight(int right) {
        this.right = right;
    }

    public int getBottom() {
        return bottom;
    }

    public void setBottom(int bottom) {
        this.bottom = bottom;
    }

    public float getCenterX() {
        return centerX;
    }

    public void setCenterX(float centerX) {
        this.centerX = centerX;
    }

    public float getCenterY() {
        return centerY;
    }

    public void setCenterY(float centerY) {
        this.centerY = centerY;
    }

    public float getWidth() {
        return width;
    }

    public void setWidth(float width) {
        this.width = width;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public Path getShapePath() {
        return path;
    }

    public abstract void setMinimumValue();
    public abstract void setShapeInfo(Component targetComponent, ComponentContainer parent, int offset, Context context);
    public abstract boolean isTouchEventInsideTheHint(TouchEvent event);
    public abstract void draw(Canvas canvas);
}
