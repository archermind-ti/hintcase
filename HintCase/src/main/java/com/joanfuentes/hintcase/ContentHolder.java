package com.joanfuentes.hintcase;

import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.app.Context;

public abstract class ContentHolder {
    public abstract Component getComponent(Context context, HintCase hintCase, ComponentContainer parent);

    public void onArrange() { }
}
